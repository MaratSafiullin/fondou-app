package nz.fondou.fondou;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import nz.fondou.fondou.DataModel.Order;
import nz.fondou.fondou.DataModel.MenuCategory;
import nz.fondou.fondou.DataModel.Option;
import nz.fondou.fondou.DataModel.CurrentMenuItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewOrderFragment extends Fragment {
    //data model object
    protected DataModel dataModel;

    //**********************************************************************************************

    //ordering state constants
    protected static final int ORDER_TYPE_NONE = 0, ORDER_TYPE_SIMPLE = 1, ORDER_TYPE_ADVANCED = 2;
    //layout state constants to switch between diffetent fragment views
    protected static final int LAYOUT_STATE_START_ORDER = 0, LAYOUT_STATE_CREATE_ORDER = 1, LAYOUT_STATE_CHOOSE_ITEM = 2, LAYOUT_STATE_ADD_ITEM = 3;

    //**********************************************************************************************

    //current state
    protected int layoutState = LAYOUT_STATE_START_ORDER;
    //current ordering state
    protected int orderType = ORDER_TYPE_NONE;
    //current new order object
    protected Order currentOrder;
    //menu object (read from local DB when making new orders)
    protected ArrayList<MenuCategory> menu;
    //current item being added to new order object
    protected CurrentMenuItem currentMenuItem;
    //options for new order item count field
    private Integer[] countValues = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    //**********************************************************************************************

    //button click listeners
    private View.OnClickListener btnNewOrderListener;
    protected View.OnClickListener btnCancelOrderListener;
    private View.OnClickListener btnPostOrderListener;
    private View.OnClickListener btnAddItemListener;
    protected View.OnClickListener btnCancelItemSelectListener;
    protected View.OnClickListener btnCancelItemAddListener;
    private View.OnClickListener btnConfirmItemAddListener;
    private class DelOrderItemClickListener implements View.OnClickListener{
        private final int itemPosition;

        public DelOrderItemClickListener(int itemPosition) {
            this.itemPosition = itemPosition;
        }

        @Override
        public void onClick(View view) {
            //remove item from current new order
            currentOrder.items.remove(itemPosition);
            refreshOrderView();
        }
    }
    //date and time edit tap listeners
    private View.OnTouchListener editDeliveryDateTouchListener;
    private View.OnTouchListener editDeliveryTimeTouchListener;
    //menu list click (item choose) listener
    private ExpandableListView.OnChildClickListener menuItemClickListener;
    //option values spinner select listener
    private class OptionValueClickListener implements AdapterView.OnItemSelectedListener {
        private final Option option;

        public OptionValueClickListener(Option option) {
            this.option = option;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            option.currentValuePosition = position;
            refreshAddedItemView();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
    //items count spinner select listener
    private AdapterView.OnItemSelectedListener spinnerCountSelectListener;

    //**********************************************************************************************

    /**
     * Adapter to fill item count spinner
     */
    protected class CountValuesAdapter extends ArrayAdapter<Integer> {
        //Context
        protected final Context context;
        //List of values
        protected final Integer[] values;

        public CountValuesAdapter(Context context, Integer[] values) {
            super(context, R.layout.row_layout_simplespinner, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            //set text
            TextView textItemName = (TextView) rowView;
            textItemName.setText(String.valueOf(values[position]));

            return textItemName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }

    /**
     * Adapter to fill Order items list
     */
    protected class OrderItemsAdapter extends ArrayAdapter<Order.OrderItem> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<Order.OrderItem> values;

        public OrderItemsAdapter(Context context, ArrayList<Order.OrderItem> values) {
            super(context, R.layout.row_order_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //
            View rowView = inflater.inflate(R.layout.row_order_item, parent, false);
            //set text
            TextView textName = (TextView) rowView.findViewById(R.id.textItemName);
            textName.setText(values.get(position).name);
            TextView textCount = (TextView) rowView.findViewById(R.id.textItemCount);
            textCount.setText(String.valueOf(values.get(position).count));
            TextView textPrice = (TextView) rowView.findViewById(R.id.textItemPrice);
            textPrice.setText(NumberFormat.getCurrencyInstance().format(values.get(position).totalPrice));
            TextView textDescription = (TextView) rowView.findViewById(R.id.textItemDescription);
            //description
            if (values.get(position).options.size() > 0) {
                String description = values.get(position).options.get(0).optionName + " = " + values.get(position).options.get(0).value;
                for (int i = 1; i < values.get(position).options.size(); i++) {
                    description = description + " | " + values.get(position).options.get(i).optionName +
                            " = " + values.get(position).options.get(i).value;
                }
                textDescription.setText(description);
            } else {
                textDescription.setVisibility(View.GONE);
            }
            //assign del button click listener
            ImageButton btnDel = (ImageButton) rowView.findViewById(R.id.buttonDeleteItem);
            btnDel.setOnClickListener(new DelOrderItemClickListener(position));
            //
            rowView.setOnClickListener(null);

            return rowView;
        }
    }

    /**
     * Adapter to fill menu list
     */
    public class MenuAdapter extends BaseExpandableListAdapter {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<MenuCategory> values;

        public MenuAdapter(Context context, ArrayList<MenuCategory> values){
            this.context = context;
            this.values = values;
        }

        @Override
        public int getGroupCount() {
            return values.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return values.get(groupPosition).items.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return values.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return values.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return values.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return values.get(groupPosition).items.get(childPosition).id;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_menu_category, null);
            }
            //set text
            TextView textViewName = (TextView) convertView.findViewById(R.id.textCategoryName);
            textViewName.setText(values.get(groupPosition).name);

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_menu_item, null);
            }
            //set text
            TextView textItemName = (TextView) convertView.findViewById(R.id.textItemName);
            TextView textItemPrice = (TextView) convertView.findViewById(R.id.textItemPrice);
            textItemName.setText(values.get(groupPosition).items.get(childPosition).name);
            textItemPrice.setText(NumberFormat.getCurrencyInstance().format(values.get(groupPosition).items.get(childPosition).basePrice));

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    /**
     * Adapter to fill item options list
     */
    protected class ItemOptionsAdapter extends ArrayAdapter<Option> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<Option> values;

        public ItemOptionsAdapter(Context context, ArrayList<Option> values) {
            super(context, R.layout.row_item_option, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //
            View rowView = inflater.inflate(R.layout.row_item_option, parent, false);
            //set text
            TextView textName = (TextView) rowView.findViewById(R.id.textOptionName);
            textName.setText(values.get(position).name);
            //set spinner adapter for option values
            Spinner spinnerValues = (Spinner) rowView.findViewById(R.id.spinnerOptionValues);
            spinnerValues.setAdapter(new OptionValuesAdapter(NewOrderFragment.this.getActivity(), values.get(position).values));
            spinnerValues.setOnItemSelectedListener (new OptionValueClickListener(values.get(position)));
            //
            rowView.setOnClickListener(null);

            return rowView;
        }
    }

    /**
     * Adapter to fill item option values spinner
     */
    protected class OptionValuesAdapter extends ArrayAdapter<Option.OptionValue> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<Option.OptionValue> values;

        public OptionValuesAdapter(Context context, ArrayList<Option.OptionValue> values) {
            super(context, R.layout.row_layout_simplespinner, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_layout_simplespinner, parent, false);
            TextView textItemName = (TextView) rowView;
            //set text
            String text = values.get(position).value;
            //if price modifier is not zero show its value
            if (values.get(position).priceModifier.compareTo(new BigDecimal("0")) != 0){
                String sign;
                BigDecimal modifier;
                if (values.get(position).priceModifier.compareTo(new BigDecimal("0")) > 0) {
                    sign = "+";
                    modifier = values.get(position).priceModifier;
                } else {
                    sign = "-";
                    modifier = values.get(position).priceModifier.negate();
                }
                text = text + " (" + sign + NumberFormat.getCurrencyInstance().format(modifier) + ")";
            }
            //
            textItemName.setText(text);

            return textItemName;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }

    //**********************************************************************************************

    /**
     * Task to post new order and save in in local DB
     */
    protected class OrderPoster extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(NewOrderFragment.this.getActivity());
            progressDialog.setMessage("Posting order...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - posting result
            //current order object is used
            //returns http response as JSON object or null
            try {
                return dataModel.postOrderToServer(currentOrder);
            } catch (Exception e) {
                e.printStackTrace();
                //show warning if order posting is unsuccessful
                NewOrderFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog alertDialog;
                        alertDialog = new AlertDialog.Builder(NewOrderFragment.this.getActivity()).create();
                        alertDialog.setTitle("Error!");
                        alertDialog.setMessage("Could not post order. Make sure you have connection. " +
                                "If this error keeps appearing cancel order, try to refresh credentials and menu info and order again");
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
                        alertDialog.show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if ((o != null)) {
                    //show notification (inform user if order needs deposit)
                    checkOrderPrice((JSONObject) o);
                    //save new order to local DB
                    dataModel.saveNewOrderToDB((JSONObject) o);
                    //switch back to new order view
                    switchLayoutState(LAYOUT_STATE_START_ORDER);
                    orderType = ORDER_TYPE_NONE;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(NewOrderFragment.this.getActivity(), "ERROR! Cannot save posted order to DB", Toast.LENGTH_LONG).show();
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    //**********************************************************************************************

    public NewOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set data model for easy access
        dataModel = ((MainActivity) this.getActivity()).dataModel;

        //on clicking new valueOrder button create new valueOrder
        btnNewOrderListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View fragmentView = NewOrderFragment.this.getView();
                ListView listOrderItems = (ListView) fragmentView.findViewById(R.id.listOrderItems);
                ExpandableListView listMenu = (ExpandableListView) fragmentView.findViewById(R.id.listMenu);
                //
                int id = view.getId();
                try {
                    if (id == R.id.buttonSimpleOrder) {
                        orderType = ORDER_TYPE_SIMPLE;
                        refreshMenu(false);
                        currentOrder = new Order(false);
                        currentOrder.timeFrame = dataModel.settings.minimumTimeFrameSimple;
                        currentOrder.deliveryTime = getSimpleOrderDefaultTime();
                    } else if (id == R.id.buttonAdvancedOrder) {
                        orderType = ORDER_TYPE_ADVANCED;
                        refreshMenu(true);
                        currentOrder = new Order(true);
                        currentOrder.deliveryTime = getAdvancedOrderDefaultTime();
                    }
                    listOrderItems.setAdapter(new OrderItemsAdapter(NewOrderFragment.this.getActivity(), currentOrder.items));
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(NewOrderFragment.this.getActivity(), "ERROR! Cannot read menu", Toast.LENGTH_LONG).show();
                    listMenu.setAdapter((MenuAdapter) null);
                }
                adjustOrderView();
                refreshOrderView();
                switchLayoutState(LAYOUT_STATE_CREATE_ORDER);
            }
        };

        //cancel valueOrder
        btnCancelOrderListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) NewOrderFragment.this.getActivity()).hideKeyboard();

                orderType = ORDER_TYPE_NONE;
                currentOrder = null;
                switchLayoutState(LAYOUT_STATE_START_ORDER);
            }
        };

        //finalize valueOrder
        btnPostOrderListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) NewOrderFragment.this.getActivity()).hideKeyboard();
                //
                Calendar calendar = Calendar.getInstance();
                //
                currentOrder.orderTime = new Date();
                currentOrder.timeFrame = (currentOrder.deliveryTime.getTime() / 1000 / 60 - currentOrder.orderTime.getTime() / 1000 / 60);
                //
                if (currentOrder.items.size() == 0) {
                    Toast.makeText(NewOrderFragment.this.getActivity(), "Order should have items", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!currentOrder.advanced) {
                    if (currentOrder.timeFrame < dataModel.settings.minimumTimeFrameSimple) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "Time between order placement and picking up is too short", Toast.LENGTH_LONG).show();
                        return;
                    }
                    calendar.setTime(currentOrder.orderTime);
                    calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingStartHour);
                    calendar.set(Calendar.MINUTE, dataModel.settings.orderingStartMinute);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    Date minimumOrderTime = calendar.getTime();
                    calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingFinishHour);
                    calendar.set(Calendar.MINUTE, dataModel.settings.orderingFinishMinute);
                    Date maximumPickupTime = calendar.getTime();
                    if (currentOrder.orderTime.getTime() < minimumOrderTime.getTime()) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "It is too early to place orders", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (currentOrder.deliveryTime.getTime() > maximumPickupTime.getTime()) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "Pick up time is too late", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    calendar.setTime(currentOrder.orderTime);
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    calendar.add(Calendar.DATE, 2);
                    Date minimalDeliveryDate = calendar.getTime();
                    calendar.setTime(currentOrder.deliveryTime);
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 0);
                    calendar.set(Calendar.MILLISECOND, 0);
                    Date actualDeliveryDate = calendar.getTime();
                    calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingStartHour);
                    calendar.set(Calendar.MINUTE, dataModel.settings.orderingStartMinute);
                    Date minimumPickupTime = calendar.getTime();
                    calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingFinishHour);
                    calendar.set(Calendar.MINUTE, dataModel.settings.orderingFinishMinute);
                    Date maximumPickupTime = calendar.getTime();
                    if (actualDeliveryDate.getTime() < minimalDeliveryDate.getTime()) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "Order has too early Pick up date", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (currentOrder.deliveryTime.getTime() < minimumPickupTime.getTime()) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "Pick up time is too early", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (currentOrder.deliveryTime.getTime() > maximumPickupTime.getTime()) {
                        Toast.makeText(NewOrderFragment.this.getActivity(), "Pick up time is too late", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                AlertDialog alertDialog;
                alertDialog = new AlertDialog.Builder(NewOrderFragment.this.getActivity()).create();
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage("Post order?");
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", (DialogInterface.OnClickListener) null);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                OrderPoster orderPoster = new OrderPoster();
                                orderPoster.execute();
                            }
                        }
                );
                alertDialog.show();
            }
        };

        //add item
        btnAddItemListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) NewOrderFragment.this.getActivity()).hideKeyboard();
                //
                switchLayoutState(LAYOUT_STATE_CHOOSE_ITEM);
            }
        };

        //cancel item select
        btnCancelItemSelectListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchLayoutState(LAYOUT_STATE_CREATE_ORDER);
            }
        };

        //cancel item add
        btnCancelItemAddListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentMenuItem = null;
                switchLayoutState(LAYOUT_STATE_CHOOSE_ITEM);
            }
        };

        //confirm item add
        btnConfirmItemAddListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Order.OrderItem.ItemOption> options = new ArrayList<>();
                for (int i = 0; i < currentMenuItem.options.size(); i++) {
                    Option currentOption = currentMenuItem.options.get(i);
                    int optionId = currentOption.id;
                    String optionName = currentOption.name;
                    int valueId = currentOption.values.get(currentOption.currentValuePosition).id;
                    String value = currentOption.values.get(currentOption.currentValuePosition).value;
                    BigDecimal priceModifier = currentOption.values.get(currentOption.currentValuePosition).priceModifier;
                    Order.OrderItem.ItemOption option = new Order.OrderItem.ItemOption(optionId, optionName, valueId, value, priceModifier);
                    options.add(option);
                }
                Order.OrderItem item = new Order.OrderItem(currentMenuItem.id, currentMenuItem.count, currentMenuItem.name, currentMenuItem.basePrice, options);
                currentOrder.items.add(item);
                currentMenuItem = null;
                refreshOrderView();
                switchLayoutState(LAYOUT_STATE_CREATE_ORDER);
            }
        };

        //change delivery date
        editDeliveryDateTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ((MainActivity) NewOrderFragment.this.getActivity()).onDateEditClick(view);
                return true;
            }
        };

        //change delivery time
        editDeliveryTimeTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ((MainActivity) NewOrderFragment.this.getActivity()).onTimeEditClick(view);
                return true;
            }
        };

        //click on menu item
        menuItemClickListener = new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
                View fragmentView = NewOrderFragment.this.getView();
                TextView textAddedItemName = (TextView) fragmentView.findViewById(R.id.textAddedItemName);
                ListView listAddedItemOptions = (ListView) fragmentView.findViewById(R.id.listAddedItemOptions);
                Spinner spinnerCount = (Spinner) fragmentView.findViewById(R.id.spinnerCount);

                MenuCategory menuCategory = ((MenuAdapter) expandableListView.getExpandableListAdapter()).values.get(groupPosition);
                MenuCategory.MenuItem menuItem = ((MenuAdapter) expandableListView.getExpandableListAdapter()).values.get(groupPosition).items.get(childPosition);
                currentMenuItem = new CurrentMenuItem(menuItem, menuCategory);

                spinnerCount.setSelection(0);
                textAddedItemName.setText(currentMenuItem.name);
                listAddedItemOptions.setAdapter(new ItemOptionsAdapter(NewOrderFragment.this.getActivity(), currentMenuItem.options));
                refreshAddedItemView();
                switchLayoutState(LAYOUT_STATE_ADD_ITEM);

                return true;
            }
        };

        //spinner count select
        spinnerCountSelectListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (currentMenuItem != null) {
                    currentMenuItem.count = (Integer) (((Spinner) adapterView).getAdapter().getItem(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        };

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshStartOrderView();
        switchLayoutState(LAYOUT_STATE_START_ORDER);

        if (!(dataModel.menuSynchronized)) {
            ((MainActivity) NewOrderFragment.this.getActivity()).refreshMenu();
        }

        if ((dataModel.settings.customerId == 0) || (dataModel.settings.customerToken.equals(""))) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(NewOrderFragment.this.getActivity()).create();
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Log in or register. Ordering functions are not available");
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
            alertDialog.show();
        } else if (!dataModel.settings.customerActive) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(NewOrderFragment.this.getActivity()).create();
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Account is not active. Ordering functions are not available");
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
            alertDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_order, container, false);
        //find interface elements
        Button buttonSimpleOrder = (Button) view.findViewById(R.id.buttonSimpleOrder);
        Button buttonAdvancedOrder = (Button) view.findViewById(R.id.buttonAdvancedOrder);
        Button buttonNewOrderCancel = (Button) view.findViewById(R.id.buttonNewOrderCancel);
        Button buttonNewOrderOK = (Button) view.findViewById(R.id.buttonNewOrderOK);
        Button buttonAddItem = (Button) view.findViewById(R.id.buttonAddItem);
        Button buttonItemSelectCancel = (Button) view.findViewById(R.id.buttonItemSelectCancel);
        Button buttonAddItemCancel = (Button) view.findViewById(R.id.buttonAddItemCancel);
        Button buttonAddItemOK = (Button) view.findViewById(R.id.buttonAddItemOK);
        Spinner spinnerCount = (Spinner) view.findViewById(R.id.spinnerCount);
        EditText editDeliveryDate = (EditText) view.findViewById(R.id.editDeliveryDate);
        EditText editDeliveryTime = (EditText) view.findViewById(R.id.editDeliveryTime);
        EditText editDeliveryTimeSimple = (EditText) view.findViewById(R.id.editDeliveryTimeSimple);
        ExpandableListView listMenu = (ExpandableListView) view.findViewById(R.id.listMenu);

        //set buttons` listeners
        buttonSimpleOrder.setOnClickListener(btnNewOrderListener);
        buttonAdvancedOrder.setOnClickListener(btnNewOrderListener);
        buttonNewOrderCancel.setOnClickListener(btnCancelOrderListener);
        buttonNewOrderOK.setOnClickListener(btnPostOrderListener);
        buttonAddItem.setOnClickListener(btnAddItemListener);
        buttonItemSelectCancel.setOnClickListener(btnCancelItemSelectListener);
        buttonAddItemCancel.setOnClickListener(btnCancelItemAddListener);
        buttonAddItemOK.setOnClickListener(btnConfirmItemAddListener);
        //list listener
        listMenu.setOnChildClickListener(menuItemClickListener);
        //edit delivery time listeners
        editDeliveryDate.setOnTouchListener(editDeliveryDateTouchListener);
        editDeliveryTime.setOnTouchListener(editDeliveryTimeTouchListener);
        editDeliveryTimeSimple.setOnTouchListener(editDeliveryTimeTouchListener);
        //spinner adapter
        spinnerCount.setAdapter(new CountValuesAdapter(NewOrderFragment.this.getActivity(), countValues));
        spinnerCount.setOnItemSelectedListener(spinnerCountSelectListener);

        return view;
    }

    //**********************************************************************************************

    /**
     * Switch view of the fragment. Hide layouts of the switchable set and show one of them
     * @param state State to switch to
     */
    private void switchLayoutState(int state) {
        View view = NewOrderFragment.this.getView();
        LinearLayout layoutStartOrder = (LinearLayout) view.findViewById(R.id.layoutStartOrder);
        LinearLayout layoutNewOrder = (LinearLayout) view.findViewById(R.id.layoutNewOrder);
        LinearLayout layoutMenu = (LinearLayout) view.findViewById(R.id.layoutMenu);
        LinearLayout layoutAddItem = (LinearLayout) view.findViewById(R.id.layoutAddItem);

        layoutStartOrder.setVisibility(View.GONE);
        layoutNewOrder.setVisibility(View.GONE);
        layoutMenu.setVisibility(View.GONE);
        layoutAddItem.setVisibility(View.GONE);

        switch (state){
            case LAYOUT_STATE_START_ORDER:
                layoutStartOrder.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_CREATE_ORDER:
                layoutNewOrder.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_CHOOSE_ITEM:
                layoutMenu.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_ADD_ITEM:
                layoutAddItem.setVisibility(View.VISIBLE);
                break;
        }
        NewOrderFragment.this.layoutState = state;
    }

    /**
     * Change order view according to order type (simple/advanced)
     */
    private void adjustOrderView() {
        View view = NewOrderFragment.this.getView();
        LinearLayout layoutPickupTimeSimple = (LinearLayout) view.findViewById(R.id.layoutPickupTimeSimple);
        LinearLayout layoutPickupTimeAdvanced = (LinearLayout) view.findViewById(R.id.layoutPickupTimeAdvanced);
        layoutPickupTimeSimple.setVisibility(View.GONE);
        layoutPickupTimeAdvanced.setVisibility(View.GONE);
        //show relevant fields to set time
        if (orderType == ORDER_TYPE_SIMPLE) {
            layoutPickupTimeSimple.setVisibility(View.VISIBLE);
        } else if (orderType == ORDER_TYPE_ADVANCED) {
            layoutPickupTimeAdvanced.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Refresh menu list from local DB (according to order type)
     * @param advanced True if advanced order, false if simple
     * @throws Exception
     */
    protected void refreshMenu(boolean advanced) throws Exception{
        View fragmentView = NewOrderFragment.this.getView();
        ExpandableListView listMenu = (ExpandableListView) fragmentView.findViewById(R.id.listMenu);
        menu = dataModel.getMenu(advanced);
        listMenu.setAdapter(new MenuAdapter(NewOrderFragment.this.getActivity(), menu));
    }

    /**
     * Refresh start order view displaying actual app settings (ordering conditions part)
     */
    protected void refreshStartOrderView() {
        View view = NewOrderFragment.this.getView();
        TextView textSimpleOrderStartTime = (TextView) view.findViewById(R.id.textSimpleOrderStartTime);
        TextView textSimpleOrderFinishTime = (TextView) view.findViewById(R.id.textSimpleOrderFinishTime);
        TextView textSimpleOrderMinTimeFrame = (TextView) view.findViewById(R.id.textSimpleOrderMinTimeFrame);
        TextView textSimpleOrderMaxNoDepositPrice = (TextView) view.findViewById(R.id.textSimpleOrderMaxNoDepositPrice);
        TextView textAdvancedOrderStartTime = (TextView) view.findViewById(R.id.textAdvancedOrderStartTime);
        TextView textAdvancedOrderFinishTime = (TextView) view.findViewById(R.id.textAdvancedOrderFinishTime);
        TextView textAdvancedOrderMinTimeFrame = (TextView) view.findViewById(R.id.textAdvancedOrderMinTimeFrame);

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingStartHour);
        calendar.set(Calendar.MINUTE, dataModel.settings.orderingStartMinute);
        String startTimeStr = timeFormat.format(calendar.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, dataModel.settings.orderingFinishHour);
        calendar.set(Calendar.MINUTE, dataModel.settings.orderingFinishMinute);
        String finishTimeStr = timeFormat.format(calendar.getTime());
        textSimpleOrderStartTime.setText(startTimeStr);
        textSimpleOrderFinishTime.setText(finishTimeStr);
        textSimpleOrderMinTimeFrame.setText(String.valueOf(dataModel.settings.minimumTimeFrameSimple));
        textSimpleOrderMaxNoDepositPrice.setText(String.valueOf(dataModel.settings.maxNoDepositCostForSimpleOrder));
        textAdvancedOrderStartTime.setText(startTimeStr);
        textAdvancedOrderFinishTime.setText(finishTimeStr);
        textAdvancedOrderMinTimeFrame.setText(String.valueOf(dataModel.settings.minimumTimeFrameAdvanced));
    }

    /**
     * Refresh current new order view (field values, order items)
     */
    private void refreshOrderView() {
        View view = NewOrderFragment.this.getView();
        TextView textTotalPrice = (TextView) view.findViewById(R.id.textTotalPrice);
        ListView listOrderItems = (ListView) view.findViewById(R.id.listOrderItems);

        //recalculate total order price
        BigDecimal totalPrice = new BigDecimal("0");
        for (int i = 0; i < currentOrder.items.size(); i++) {
            Order.OrderItem item = currentOrder.items.get(i);
            BigDecimal positionPrice = item.totalPrice.multiply(new BigDecimal(item.count));
            totalPrice = totalPrice.add(positionPrice);
        }
        currentOrder.totalPrice = totalPrice;
        textTotalPrice.setText(NumberFormat.getCurrencyInstance().format(totalPrice));
        //refresh item list
        ((OrderItemsAdapter) listOrderItems.getAdapter()).notifyDataSetChanged();
        //refresh time fields
        EditText editDeliveryDate = (EditText) view.findViewById(R.id.editDeliveryDate);
        EditText editDeliveryTime = (EditText) view.findViewById(R.id.editDeliveryTime);
        EditText editDeliveryTimeSimple = (EditText) view.findViewById(R.id.editDeliveryTimeSimple);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");;
        editDeliveryDate.setText(dateFormat.format(currentOrder.deliveryTime));
        editDeliveryTime.setText(timeFormat.format(currentOrder.deliveryTime));
        editDeliveryTimeSimple.setText(timeFormat.format(currentOrder.deliveryTime));

    }

    /**
     * Refresh new order item view from current item object
     */
    private void refreshAddedItemView() {
        View view = NewOrderFragment.this.getView();
        TextView textCost = (TextView) view.findViewById(R.id.textItemCost);

        //recalculate item total cost
        BigDecimal cost = currentMenuItem.basePrice;
        for (int i = 0; i < currentMenuItem.options.size(); i++) {
            Option option = currentMenuItem.options.get(i);
            BigDecimal priceModifier = option.values.get(option.currentValuePosition).priceModifier;
            cost = cost.add(priceModifier);
        }
        textCost.setText(NumberFormat.getCurrencyInstance().format(cost));
    }

    /**
     * Check id order needs deposit and show notification
     * @param order Order object from server (actual created order)
     * @throws Exception
     */
    private void checkOrderPrice(JSONObject order) throws Exception{
        boolean advanced = order.getBoolean("advanced");
        int totalPrice = order.getInt("totalPrice");

        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(NewOrderFragment.this.getActivity()).create();
        alertDialog.setTitle("Info");
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
        //all advanced orders need deposit
        if (advanced) {
            alertDialog.setMessage("Order posted successfully. All advanced orders require deposit. Please contact fond(ou) cafe");
        }
        //simple orders need deposit only if the price is higher than special value from app settings
        else if (totalPrice > dataModel.settings.maxNoDepositCostForSimpleOrder * 100) {
            alertDialog.setMessage("Order posted successfully. Orders with Total Price more than NZD " + dataModel.settings.maxNoDepositCostForSimpleOrder +
                    " require deposit. Please contact fond(ou) cafe");
        }
        //no deposit needed
        else {
            alertDialog.setMessage("Order posted successfully");
        }
        alertDialog.show();
    }

    //**********************************************************************************************

    /**
     * Calculate default pick up time for advanced order (depends on current ordering conditions)
     * @return Default pick up time
     */
    private Date getAdvancedOrderDefaultTime() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate= format.parse(format.format(new Date()));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDate);
            calendar.add(Calendar.DATE, dataModel.settings.minimumTimeFrameAdvanced);
            calendar.add(Calendar.HOUR, dataModel.settings.orderingStartHour);
            return calendar.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    /**
     * Calculate default pick up time for simple order
     * (minimum number of minutes for preparing order + 5 minutes ahead of current time)
     * @return Default pick up time
     */
    private Date getSimpleOrderDefaultTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.MINUTE, dataModel.settings.minimumTimeFrameSimple + 5);
        return calendar.getTime();
    }
}
