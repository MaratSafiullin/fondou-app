package nz.fondou.fondou;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * fond(ou) application main activity. handles common tasks for the whole app
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //database handler
    protected FondouDatabaseHelper databaseHelper = new FondouDatabaseHelper(this);
    //data model object
    protected DataModel dataModel = new DataModel(databaseHelper);
    //targets to save result of date or time select dialog
    private TextView dialogTargetField;
    private Date dialogTargetObj;
    //dialog listeners and dialogs to select time and date
    private DatePickerDialog.OnDateSetListener datePickerCallBack;
    private TimePickerDialog.OnTimeSetListener timePickerCallBack;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    //broadcast receiver for GCM Reg Service
    BroadcastReceiver gcmRegistrationReceiver;

    //**********************************************************************************************

    //some stuff for google play services
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    //tags for app fragments to search them
    private final String FRAGMENT_TAG_ACCOUNT = "account", FRAGMENT_TAG_NEW_ORDER = "new_order",
            FRAGMENT_TAG_MY_ORDERS = "my_orders", FRAGMENT_TAG_ABOUT = "about",
            FRAGMENT_TAG_CONTACTS = "contacts", FRAGMENT_TAG_HOME = "home";
    //dialog calling constants
    private final int DIALOG_DATE = 1, DIALOG_TIME = 2;
    //flag to indicate that GCM ID is registered by app
    private boolean gcmIdRegistered = false;

    //**********************************************************************************************

    //fragments used to represent application modes, each of them replaces main content of the activity
    protected NewOrderFragment newOrderFragment = new NewOrderFragment();
    protected MyOrdersFragment myOrdersFragment = new MyOrdersFragment();
    protected AccountFragment accountFragment = new AccountFragment();
    protected AboutFragment aboutFragment = new AboutFragment();
    protected ContactsFragment contactsFragment = new ContactsFragment();
    protected HomeFragment homeFragment = new HomeFragment();

    //**********************************************************************************************

    /**
     * Task to check if customer's token is valid (modify saved app parameters)
     */
    protected class CustomerInfoRefresher extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Checking credentials...");
            progressDialog.show();
            //reset flag
            dataModel.credentialsChecked = false;
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - id/token check result
            //needs customer id and token
            //returns http response as JSON object or null
            try {
                int customerId = (Integer) objects[0];
                String customerToken = (String) objects[1];
                return dataModel.getTokenCheckResult(customerId, customerToken);
            } catch (Exception e) {
                e.printStackTrace();
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "ERROR! Cannot get credentials from server", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if (o != null) {
                    //make changes in settings
                    String[] result = dataModel.processTokenCheckResult((JSONObject) o);
                    //(re)register/unregister GCM ID
                    switch(result[0]){
                        case "OK":
                            //if possible, start GCM Registration Service to get GCM ID
                            if (checkPlayServices()) {
                                Intent intent = new Intent(MainActivity.this, GcmRegistrationService.class);
                                startService(intent);
                            }
                            break;
                        case "FAIL":
                            //erase GCM id on server
                            unregisterGcm(result[1], Integer.valueOf(result[2]), result[3]);
                            break;
                    }
                    //if token is valid
                    if ((dataModel.settings.customerId != 0) && (!dataModel.settings.customerToken.equals(""))) {
                        //if customer is not active (yet) show warning
                        if (!dataModel.settings.customerActive) {
                            AlertDialog alertDialog;
                            alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("Warning");
                            alertDialog.setMessage("Account is not active. Ordering functions are not available");
                            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
                            alertDialog.show();
                        }
                        //save current ordering conditions
                        try {
                            dataModel.applyOrderingSettingsFromResponse((JSONObject) o);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "WARNING! Could not refresh ordering settings from website. Order functions may work incorrectly", Toast.LENGTH_LONG).show();
                        }
                        //flag
                        dataModel.credentialsChecked = true;
                        //refresh account fragment (if loaded)
                        AccountFragment accountFragment = (AccountFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_ACCOUNT);
                        if (accountFragment != null) {accountFragment.refreshAccountInfo();}
                        //refresh new order fragment ordering conditions (if loaded)
                        NewOrderFragment newOrderFragment = (NewOrderFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_NEW_ORDER);
                        if (newOrderFragment != null) {newOrderFragment.refreshStartOrderView();}
                    }
                    //if token is not valid
                    else {
                        //show login part in account fragment (if loaded)
                        AccountFragment accountFragment = (AccountFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_ACCOUNT);
                        if (accountFragment != null) {MainActivity.this.accountFragment.switchLayoutState(AccountFragment.LAYOUT_STATE_LOGIN);}
                        //refresh orders list (should be cleared) in my orders fragment (if active)
                        MyOrdersFragment myOrdersFragment = (MyOrdersFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_MY_ORDERS);
                        if (myOrdersFragment != null) {myOrdersFragment.refreshOrdersList();}
                        //show warning
                        AlertDialog alertDialog;
                        alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                        alertDialog.setTitle("Warning");
                        alertDialog.setMessage("Credentials are invalid. Log in or register before you can order");
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
                        alertDialog.show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "ERROR! Cannot complete refreshing", Toast.LENGTH_LONG).show();
            } finally {
                //hide dialog
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Task to synchronize orders list with the server
     */
    protected class OrdersSynchronizer extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Synchronizing orders list with website...");
            progressDialog.show();
            //reset flag
            dataModel.ordersSynchronized = false;
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - changes made after the previous check
            //returns http response as JSON object or null
            try {
                return dataModel.getOrdersFromServer();
            } catch (Exception e) {
                e.printStackTrace();
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "ERROR! Cannot get orders list from website", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if ((o != null)) {
                    //save changes to local DB
                    dataModel.saveOrdersToDB((JSONObject) o);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "ERROR! Cannot synchronize orders list", Toast.LENGTH_LONG).show();
            } finally {
                //hide dialog
                progressDialog.dismiss();
                //refresh orders list in my orders fragment (if active)
                //even if synchronization failed still display what is in local DB
                MyOrdersFragment myOrdersFragment = (MyOrdersFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_MY_ORDERS);
                if (myOrdersFragment != null) {myOrdersFragment.refreshOrdersList();}
            }
        }
    }

    /**
     * Task to synchronize local menu with the server
     */
    protected class MenuSynchronizer extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Synchronizing menu with website...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - changes made after the previous check
            //returns http response as JSON object or null
            try {
                return dataModel.getMenuFromServer();
            } catch (Exception e) {
                e.printStackTrace();
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MainActivity.this, "ERROR! Cannot get menu from website", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if ((o != null)) {
                    //save changes to local DB
                    dataModel.saveMenuToDB((JSONObject) o);
                    //if  make order fragment is loaded refresh menu
                    NewOrderFragment newOrderFragment = (NewOrderFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_NEW_ORDER);
                    if (newOrderFragment != null) {
                        try {
                            //load from local DB menu part according to current order type
                            if (newOrderFragment.orderType == NewOrderFragment.ORDER_TYPE_SIMPLE) {
                                newOrderFragment.refreshMenu(false);
                            } else if (newOrderFragment.orderType == NewOrderFragment.ORDER_TYPE_ADVANCED) {
                                newOrderFragment.refreshMenu(true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "ERROR! Cannot synchronize menu", Toast.LENGTH_LONG).show();
            } finally {
                //hide dialog
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Task to send received GCM id to server to be registered for current customer
     */
    protected class GcmRegistrator extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects){
            //send GCM id to server, save setting
            //http response from server is ignored
            //it is supposed that attempt is made every time when user info is refreshed and token check is successful
            //on server side request is ignored if already registered
            try {
                String gcmId = (String) objects[0];
                dataModel.registerGcmId(gcmId);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return null;
            }
        }
    }

    /**
     * Task to erase locally saved GCM id for current customer on server
     */
    protected class GcmUnregistrator extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects){
            //erase GCM id on server, save setting
            //needs customer id and token (cannot take from local settings as they may be cleared by parallel process)
            //http response is ignored
            //if it is unsuccessful the app will still receive GCM notifications (e.g. after log out), but will ignore them
            try {
                String gcmId = (String) objects[0];
                int customerId = (Integer) objects[1];
                String customerToken = (String) objects[2];
                dataModel.unregisterGcmId(gcmId, customerId, customerToken);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return null;
            }
        }
    }

    //**********************************************************************************************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize action bar
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //initialize navigation drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //initialize drawer menu
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //initialize date picker dialog listener
        datePickerCallBack = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //save new value to target obj
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dialogTargetObj);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dialogTargetObj.setTime(calendar.getTime().getTime());
                //save new value to target field
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                dialogTargetField.setText(dateFormat.format(dialogTargetObj));
            }
        };
        //initialize date picker dialog
        datePickerDialog = new DatePickerDialog(this, datePickerCallBack, 0, 0, 0);
        //initialize time picker dialog listener
        timePickerCallBack = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                //save new value to target obj
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dialogTargetObj);
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                dialogTargetObj.setTime(calendar.getTime().getTime());
                //save new value to target field
                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aaa");
                dialogTargetField.setText(timeFormat.format(dialogTargetObj));
            }
        };
        //initialize time picker dialog
        timePickerDialog = new TimePickerDialog(this, timePickerCallBack, 0, 0, false);

        //read current settings from local DB
        try {
            dataModel.readSettings();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "ERROR! Cannot read application settings. " +
                    "Application cannot work correctly. Please restart", Toast.LENGTH_LONG).show();
        }
        //if user is logged in refresh its info from server
        if ((dataModel.settings.customerId != 0) && (!dataModel.settings.customerToken.equals(""))) {
            refreshCustomerInfo();
        }

        //register receiver for broadcasts from GCM Registration Service
        gcmRegistrationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!MainActivity.this.gcmIdRegistered) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                    boolean success = sharedPreferences.getBoolean(PreferencesAndConstants.GCM_REGISTRATION_SUCCESSFUL, false);
                    //if registered successfully send GCM id to server
                    if (success) {
                        String token = sharedPreferences.getString(PreferencesAndConstants.GCM_ID, "");
                        registerGcm(token);
                        MainActivity.this.gcmIdRegistered = true;
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(gcmRegistrationReceiver, new IntentFilter(PreferencesAndConstants.REGISTRATION_COMPLETE));

        //if launched by tapping notification go to "my orders", otherwise load home fragment
        Intent intent = getIntent();
        boolean isOrderStatusChangeNotification = intent.getBooleanExtra(PreferencesAndConstants.ORDER_STATUS_CHANGE_NOTIFICATION, false);
        if (isOrderStatusChangeNotification) {
            switchFragment(R.id.nav_my_orders);
        } else {
            switchFragment(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unregister receiver for broadcasts from GCM Registration Service
        LocalBroadcastManager.getInstance(this).unregisterReceiver(gcmRegistrationReceiver);
    }

    @Override
    public void onBackPressed() {
        //hide nav drawer if open
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        //
        else {
            //check which fragment is loaded (except home fragment)
            AccountFragment accountFragment = (AccountFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_ACCOUNT);
            NewOrderFragment newOrderFragment = (NewOrderFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_NEW_ORDER);
            MyOrdersFragment myOrdersFragment = (MyOrdersFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_MY_ORDERS);
            AboutFragment aboutFragment = (AboutFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_ABOUT);
            ContactsFragment contactsFragment = (ContactsFragment) MainActivity.this.getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_CONTACTS);
            //if make order fragment cancel current action or go back to home fragment
            if (newOrderFragment != null) {
                switch (newOrderFragment.layoutState) {
                    case NewOrderFragment.LAYOUT_STATE_START_ORDER:
                        switchFragment(0);
                        break;
                    case NewOrderFragment.LAYOUT_STATE_CREATE_ORDER:
                        newOrderFragment.btnCancelOrderListener.onClick(null);
                        break;
                    case NewOrderFragment.LAYOUT_STATE_CHOOSE_ITEM:
                        newOrderFragment.btnCancelItemSelectListener.onClick(null);
                        break;
                    case NewOrderFragment.LAYOUT_STATE_ADD_ITEM:
                        newOrderFragment.btnCancelItemAddListener.onClick(null);
                        break;
                }
            }
            //if account fragment cancel info editing or go back to home fragment
            else if (accountFragment != null) {
                if (accountFragment.layoutState == AccountFragment.LAYOUT_STATE_EDIT_INFO) {
                    accountFragment.btnGoInfoListener.onClick(null);
                } else {
                    switchFragment(0);
                }
            }
            //for others go back to home fragment
            else if (myOrdersFragment != null) {
                switchFragment(0);
            } else if (aboutFragment != null) {
                switchFragment(0);
            } else if (contactsFragment != null) {
                switchFragment(0);
            }
            //in other cases do default (close acivity)
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //action bar menu, serves for refreshing purposes
        switch (item.getItemId()) {
            case R.id.action_refresh_credentials:
                refreshCustomerInfo();
                return true;
            case R.id.action_refresh_menu:
                refreshMenu();
                return true;
            case R.id.action_refresh_orders:
                refreshOrders();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        // go to selected fragment
        switchFragment(id);
        // close nav srawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        //show date or time picker dialog
        if (id == DIALOG_DATE) {
            return datePickerDialog;
        } else if (id == DIALOG_TIME) {
            return timePickerDialog;
        }
        //
        return super.onCreateDialog(id);
    }

    //**********************************************************************************************

    /**
     * Switch fragment in the main container
     * @param itemId Id of the menu item clicked
     */
    protected void switchFragment(int itemId){
        hideKeyboard();
        //show chosen fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (itemId){
            case R.id.nav_new_order:
                fragmentTransaction.replace(R.id.layoutMainContainer, newOrderFragment, FRAGMENT_TAG_NEW_ORDER);
                break;
            case R.id.nav_my_orders:
                fragmentTransaction.replace(R.id.layoutMainContainer, myOrdersFragment, FRAGMENT_TAG_MY_ORDERS);
                break;
            case R.id.nav_account:
                fragmentTransaction.replace(R.id.layoutMainContainer, accountFragment, FRAGMENT_TAG_ACCOUNT);
                break;
            case R.id.nav_about:
                fragmentTransaction.replace(R.id.layoutMainContainer, aboutFragment, FRAGMENT_TAG_ABOUT);
                break;
            case R.id.nav_contacts:
                fragmentTransaction.replace(R.id.layoutMainContainer, contactsFragment, FRAGMENT_TAG_CONTACTS);
                break;
            default:
                fragmentTransaction.replace(R.id.layoutMainContainer, homeFragment, FRAGMENT_TAG_HOME);
                break;
        }
        fragmentTransaction.commit();
    }

    /**
     * hide keyboard
     */
    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.findViewById(R.id.layoutMainContainer).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //**********************************************************************************************

    /**
     * Tap on date selection edit
     * @param view Tapped Edit
     */
    protected void onDateEditClick(View view) {
        //set dialog target
        dialogTargetField = (TextView) view;
        switch (view.getId()){
            case R.id.editDeliveryDate:
                dialogTargetObj = newOrderFragment.currentOrder.deliveryTime;
                break;
            default:
                return;
        }
        //get current data
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dialogTargetObj);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        //initialize dialog
        datePickerDialog.getDatePicker().init(year, month, day, null);
        datePickerDialog.setTitle("Set Date");
        //
        showDialog(DIALOG_DATE);
    }

    /**
     * Tap on time selection edit
     * @param view Tapped Edit
     */
    protected void onTimeEditClick(View view) {
        //set dialog target
        dialogTargetField = (TextView) view;
        switch (view.getId()){
            case R.id.editDeliveryTime:
                dialogTargetObj = newOrderFragment.currentOrder.deliveryTime;
                break;
            case R.id.editDeliveryTimeSimple:
                dialogTargetObj = newOrderFragment.currentOrder.deliveryTime;
                break;
            default:
                return;
        }
        //get current data
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dialogTargetObj);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        //initialize dialog
        timePickerDialog.updateTime(hour, minute);
        timePickerDialog.setTitle("Set Time");
        //
        showDialog(DIALOG_TIME);
    }

    //**********************************************************************************************

    /**
     * Refresh current customer info, get from server (check token validity)
     */
    protected void refreshCustomerInfo() {
        CustomerInfoRefresher refresher = new CustomerInfoRefresher();
        refresher.execute(dataModel.settings.customerId, dataModel.settings.customerToken);
    }

    /**
     * Refresh current customer's orders, get from server
     */
    protected void refreshOrders() {
        OrdersSynchronizer ordersSynchronizer = new OrdersSynchronizer();
        ordersSynchronizer.execute();
    }

    /**
     * Refresh menu info, get from server
     */
    protected void refreshMenu() {
        MenuSynchronizer menuSynchronizer = new MenuSynchronizer();
        menuSynchronizer.execute();
    }

    /**
     * Send request to register GCM id on server
     */
    protected void registerGcm(String gcmId) {
        GcmRegistrator gcmRegistrator = new GcmRegistrator();
        gcmRegistrator.execute(gcmId);
    }

    /**
     * Send request to register GCM id on server
     */
    protected void unregisterGcm(String gcmId, int customerId, String customerToken) {
        GcmUnregistrator gcmUnregistrator = new GcmUnregistrator();
        gcmUnregistrator.execute(gcmId, customerId, customerToken);
    }

    //**********************************************************************************************

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    protected boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
