package nz.fondou.fondou;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A "homepage" fragment of the app. Links to app's most used functions
 */
public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // button to go to "new order" fragment
        Button buttonHomeOrder = (Button) view.findViewById(R.id.buttonHomeOrder);
        buttonHomeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) HomeFragment.this.getActivity()).switchFragment(R.id.nav_new_order);
            }
        });

        // button to go to "my orders" fragment
        Button buttonHomeMyOrders = (Button) view.findViewById(R.id.buttonHomeMyOrders);
        buttonHomeMyOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) HomeFragment.this.getActivity()).switchFragment(R.id.nav_my_orders);
            }
        });

        return view;
    }

}
