package nz.fondou.fondou;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import nz.fondou.fondou.DataModel.PostedOrder;

/**
 * A fragment to display posted orders of different statuses
 */
public class MyOrdersFragment extends Fragment {
    //data model object
    protected DataModel dataModel;

    //**********************************************************************************************

    //layout state constants to switch between diffetent fragment views
    protected static final int LAYOUT_STATE_NEW = 0, LAYOUT_STATE_PROCESSED = 1, LAYOUT_STATE_READY = 2;

    //**********************************************************************************************

    //current state
    protected int layoutState = LAYOUT_STATE_NEW;

    //**********************************************************************************************

    //button click listeners
    private View.OnClickListener btnNewOrdersListener;
    private View.OnClickListener btnProcessedOrdersListener;
    private View.OnClickListener btnReadyOrdersListener;
    private class CancelOrderClickListener implements View.OnClickListener {
        //id of the order-to-cancel record
        private final int orderId;

        public CancelOrderClickListener(int orderId) {
            this.orderId = orderId;
        }

        @Override
        public void onClick(View view) {
            //if confirmed cancel the order
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(MyOrdersFragment.this.getActivity()).create();
            alertDialog.setTitle("Confirm");
            alertDialog.setMessage("Do you want to cancel the order?");
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", (DialogInterface.OnClickListener) null);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            OrderCanceller orderCanceller = new OrderCanceller();
                            orderCanceller.execute(CancelOrderClickListener.this.orderId);
                        }
                    }
            );
            alertDialog.show();
        }
    }

    //**********************************************************************************************

    /**
     * Adapter to fill orders list
     */
    protected class OrdersAdapter extends ArrayAdapter<PostedOrder> {
        //Context
        protected final Context context;
        //List of values
        protected final ArrayList<PostedOrder> values;
        //Read only orders (no cancelling)
        protected final boolean readOnly;

        public OrdersAdapter(Context context, ArrayList<PostedOrder> values, boolean readOnly) {
            super(context, R.layout.row_posted_order, values);
            this.context = context;
            this.values = values;
            this.readOnly = readOnly;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_posted_order, parent, false);

            //hide cancel button if readonly, otherwise set listener
            Button buttonCancel = (Button) rowView.findViewById(R.id.buttonCancelOrder);
            if (readOnly) {
                buttonCancel.setVisibility(View.GONE);
            } else {
                buttonCancel.setOnClickListener(new CancelOrderClickListener(values.get(position).id));
            }

            Format format = new SimpleDateFormat("MMM-dd hh:mm aaa");
            //set text
            TextView textOrderTime = (TextView) rowView.findViewById(R.id.textOrderTime);
            textOrderTime.setText(format.format(values.get(position).orderTime));
            TextView textDeliveryTime = (TextView) rowView.findViewById(R.id.textDeliveryTime);
            textDeliveryTime.setText(format.format(values.get(position).deliveryTime));
            TextView textTotalPrice = (TextView) rowView.findViewById(R.id.textTotalPrice);
            textTotalPrice.setText(NumberFormat.getCurrencyInstance().format(values.get(position).totalPrice));

            //show "deposit" label if the order is advanced or order price is bigger that maximum-price-with-no-deposit setting value
            if (parent.getId() == R.id.listNewOrders) {
                TextView textDepositNeeded = (TextView) rowView.findViewById(R.id.textDepositNeeded);
                if (values.get(position).advanced) {
                    textDepositNeeded.setVisibility(View.VISIBLE);
                } else {
                    BigDecimal maxNoDepositCostForSimpleOrder = new BigDecimal(dataModel.settings.maxNoDepositCostForSimpleOrder);
                    if (values.get(position).totalPrice.compareTo(maxNoDepositCostForSimpleOrder) == 1) {
                        textDepositNeeded.setVisibility(View.VISIBLE);
                    }
                }
            }

            //fill list of order items
            LinearLayout layoutOrderItemsList = (LinearLayout) rowView.findViewById(R.id.layoutOrderItemsList);
            for (int i = 0; i < values.get(position).items.size(); i++) {
                PostedOrder.PostedOrderItem item = values.get(position).items.get(i);
                //create view from row layout
                View itemRowView = LayoutInflater.from(context).inflate(R.layout.row_posted_order_item, layoutOrderItemsList, false);

                //set text
                TextView textCount = (TextView) itemRowView.findViewById(R.id.textItemCount);
                textCount.setText(String.valueOf(item.count));
                TextView textName = (TextView) itemRowView.findViewById(R.id.textItemName);
                textName.setText(item.name);
                TextView textPrice = (TextView) itemRowView.findViewById(R.id.textItemPrice);
                textPrice.setText(NumberFormat.getCurrencyInstance().format(item.price));
                TextView textDescription = (TextView) itemRowView.findViewById(R.id.textItemDescription);
                if (!item.description.equals("")) {
                    textDescription.setText(item.description);
                } else {
                    textDescription.setVisibility(View.GONE);
                }

                //add view to order items list layout
                layoutOrderItemsList.addView(itemRowView);
            }

            return rowView;
        }
    }

    //**********************************************************************************************

    /**
     * Task to cancel order
     */
    protected class OrderCanceller extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(MyOrdersFragment.this.getActivity());
            progressDialog.setMessage("Cancelling order...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - order cancel result
            //needs Order Id
            //returns http response as JSON object or null
            try {
                int orderId = (Integer) objects[0];
                JSONObject cancelResult = dataModel.cancelOrderOnServer(orderId);
                cancelResult.put("orderId", orderId);
                return cancelResult;
            } catch (Exception e) {
                e.printStackTrace();
                MyOrdersFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MyOrdersFragment.this.getActivity(), "ERROR! Cannot get response from website", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if ((o != null)) {
                    //delete from local DB if cancelled successfully
                    if (!dataModel.processCancelledOrder((JSONObject) o)) {
                        Toast.makeText(MyOrdersFragment.this.getActivity(), "ERROR! Order was not cancelled on website", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MyOrdersFragment.this.getActivity(), "ERROR! Cannot delete cancelled order", Toast.LENGTH_LONG).show();
            } finally {
                //hide
                progressDialog.dismiss();
                //refresh orders list in fragment
                refreshOrdersList();
            }
        }
    }

    //**********************************************************************************************

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set data model for easy access
        dataModel = ((MainActivity) this.getActivity()).dataModel;

        //switch to new orders view
        btnNewOrdersListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchLayoutState(LAYOUT_STATE_NEW);
            }
        };

        //switch to processed orders view
        btnProcessedOrdersListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchLayoutState(LAYOUT_STATE_PROCESSED);
            }
        };

        //switch to ready orders view
        btnReadyOrdersListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchLayoutState(LAYOUT_STATE_READY);
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //switch to new (pending) orders view
        switchLayoutState(LAYOUT_STATE_NEW);

        //if orders were not synchronized yet (after app launch) then synchronize them with server and show fresh data
        //otherwise just show data from local DB
        if (!(dataModel.ordersSynchronized)) {
            ((MainActivity) MyOrdersFragment.this.getActivity()).refreshOrders();
        } else {
            refreshOrdersList();
        }

        //show warning if user is not logged in
        if ((dataModel.settings.customerId == 0) || (dataModel.settings.customerToken.equals(""))) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(MyOrdersFragment.this.getActivity()).create();
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Log in or register. Ordering functions are not available");
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
            alertDialog.show();
        }
        //show warning if account is not activated (yet)
        else if (!dataModel.settings.customerActive) {
            AlertDialog alertDialog;
            alertDialog = new AlertDialog.Builder(MyOrdersFragment.this.getActivity()).create();
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Account is not active. Ordering functions are not available");
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
            alertDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_orders, container, false);
        //find interface elements
        Button buttonNewOrders = (Button) view.findViewById(R.id.buttonNewOrders);
        Button buttonProcessedOrders = (Button) view.findViewById(R.id.buttonProcessedOrders);
        Button buttonReadyOrders = (Button) view.findViewById(R.id.buttonReadyOrders);

        //set buttons` listeners
        buttonNewOrders.setOnClickListener(btnNewOrdersListener);
        buttonProcessedOrders.setOnClickListener(btnProcessedOrdersListener);
        buttonReadyOrders.setOnClickListener(btnReadyOrdersListener);

        return view;
    }

    //**********************************************************************************************

    /**
     * Refresh orders list from local DB
     */
    protected void refreshOrdersList() {
        try {
            //find interface elements
            ListView listNewOrders = (ListView) MyOrdersFragment.this.getView().findViewById(R.id.listNewOrders);
            ListView listProcessedOrders = (ListView) MyOrdersFragment.this.getView().findViewById(R.id.listProcessedOrders);
            ListView listReadyOrders = (ListView) MyOrdersFragment.this.getView().findViewById(R.id.listReadyOrders);

            //set lists adapters
            listNewOrders.setAdapter(new OrdersAdapter(MyOrdersFragment.this.getActivity(), dataModel.getOrdersList(dataModel.ORDER_STATUS_CREATED), false));
            listProcessedOrders.setAdapter(new OrdersAdapter(MyOrdersFragment.this.getActivity(), dataModel.getOrdersList(dataModel.ORDER_STATUS_PROCESSED), true));
            listReadyOrders.setAdapter(new OrdersAdapter(MyOrdersFragment.this.getActivity(), dataModel.getOrdersList(dataModel.ORDER_STATUS_SENT), true));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MyOrdersFragment.this.getActivity(), "ERROR! Cannot read orders list", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Switch view of the fragment. Hide layouts of the switchable set and show one of them
     * @param state State to switch to
     */
    private void switchLayoutState(int state){
        View view = MyOrdersFragment.this.getView();
        LinearLayout layoutNewOrders = (LinearLayout) view.findViewById(R.id.layoutNewOrders);
        LinearLayout layoutProcessedOrders = (LinearLayout) view.findViewById(R.id.layoutProcessedOrders);
        LinearLayout layoutReadyOrders = (LinearLayout) view.findViewById(R.id.layoutReadyOrders);

        layoutNewOrders.setVisibility(View.GONE);
        layoutProcessedOrders.setVisibility(View.GONE);
        layoutReadyOrders.setVisibility(View.GONE);

        switch (state){
            case LAYOUT_STATE_NEW:
                layoutNewOrders.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_PROCESSED:
                layoutProcessedOrders.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_READY:
                layoutReadyOrders.setVisibility(View.VISIBLE);
                break;
        }
        MyOrdersFragment.this.layoutState = state;
    }
}
