package nz.fondou.fondou;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Class to implement GCM registration service (get GCM ID and return it to Main Activity to (re)register)
 */
public class GcmRegistrationService extends IntentService {
    public GcmRegistrationService() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            //get GCM ID
            InstanceID instanceID = InstanceID.getInstance(this);
            String gcmId = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            // prepare success notification and GCM ID for main activity
            sharedPreferences.edit().putBoolean(PreferencesAndConstants.GCM_REGISTRATION_SUCCESSFUL, true).apply();
            sharedPreferences.edit().putString(PreferencesAndConstants.GCM_ID, gcmId).apply();
        } catch (Exception e) {
            e.printStackTrace();
            // prepare fail notification for main activity
            sharedPreferences.edit().putBoolean(PreferencesAndConstants.GCM_REGISTRATION_SUCCESSFUL, false).apply();
        }
        // send notification to main activity
        Intent registrationComplete = new Intent(PreferencesAndConstants.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
}
