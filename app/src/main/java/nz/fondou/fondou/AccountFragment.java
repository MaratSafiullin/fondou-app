package nz.fondou.fondou;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

/**
 * A fragment to show account info, login/register, edit account info/change password
 */
public class AccountFragment extends Fragment {
    //data model object
    protected DataModel dataModel;

    //**********************************************************************************************

    //layout state constants to switch between diffetent fragment views
    protected static final int LAYOUT_STATE_LOGIN = 0, LAYOUT_STATE_REGISTER = 1, LAYOUT_STATE_ACCOUNT_INFO = 2, LAYOUT_STATE_EDIT_INFO = 3;

    //**********************************************************************************************

    //current state
    protected int layoutState = LAYOUT_STATE_LOGIN;

    //**********************************************************************************************

    //button click listeners
    private View.OnClickListener btnLoginListener;
    private View.OnClickListener btnRegisterListener;
    private View.OnClickListener btnLogoutListener;
    private View.OnClickListener btnRefreshCustomerInfoListener;
    private View.OnClickListener btnSaveCustomerInfoListener;
    private View.OnClickListener btnChangePasswordListener;
    private View.OnClickListener btnGoRegisterListener;
    private View.OnClickListener btnGoLoginListener;
    private View.OnClickListener btnGoEditListener;
    protected View.OnClickListener btnGoInfoListener;

    //**********************************************************************************************

    /**
     * Task to perform log in
     */
    protected class CustomerLoginer extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(AccountFragment.this.getActivity());
            progressDialog.setMessage("Trying to log in...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - log in result
            //needs Customer's phone and password
            //returns http response as JSON object or null
            try {
                String phone = (String) objects[0];
                String password = (String) objects[1];
                return dataModel.getLogInResult(phone, password);
            } catch (Exception e) {
                e.printStackTrace();
                AccountFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot get response from server", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if (o != null) {
                    //save account settings
                    boolean loginSucceffull = dataModel.applyCustomerSettings((JSONObject) o);
                    //If successful
                    if (loginSucceffull) {
                        //save ordering conditions
                        try {
                            dataModel.applyOrderingSettingsFromResponse((JSONObject) o);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AccountFragment.this.getActivity(), "WARNING! Could not refresh ordering settings from website. Order functions may work incorrectly", Toast.LENGTH_LONG).show();
                        }
                        //Get GCM id and register on server
                        if (((MainActivity) AccountFragment.this.getActivity()).checkPlayServices()) {
                            Intent intent = new Intent((AccountFragment.this.getActivity()), GcmRegistrationService.class);
                            (AccountFragment.this.getActivity()).startService(intent);
                        }
                        //clear orders in local DB just in case
                        dataModel.clearOrders();
                        //display account info
                        refreshAccountInfo();
                        switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
                    } else {
                        Toast.makeText(AccountFragment.this.getActivity(), "Log In failed. Check PHONE NUMBER and PASSWORD", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot complete log in", Toast.LENGTH_LONG).show();
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Task to perform new customer registration
     */
    protected class CustomerRegistrator extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(AccountFragment.this.getActivity());
            progressDialog.setMessage("Trying to register...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - registration result
            //needs customer's info
            //returns http response as JSON object or null
            try {
                String phone = (String) objects[0];
                String name = (String) objects[1];
                String email = (String) objects[2];
                String password = (String) objects[3];
                //before registration check if phone or email is already registered
                if (!dataModel.checkPhoneEmailFree(phone, email)) {
                    AccountFragment.this.getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Phone or email is already used", Toast.LENGTH_LONG).show();
                        }
                    });
                    return null;
                }
                return dataModel.getRegistrationResult(phone, name, email, password);
            } catch (Exception e) {
                e.printStackTrace();
                AccountFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot get response from server", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if (o != null) {
                    //save account settings
                    boolean registrationSuccessful = dataModel.applyCustomerSettings((JSONObject) o);
                    //If successful
                    if (registrationSuccessful) {
                        //save ordering conditions
                        try {
                            dataModel.applyOrderingSettingsFromResponse((JSONObject) o);
                            //Get GCM id and register on server
                            if (((MainActivity) AccountFragment.this.getActivity()).checkPlayServices()) {
                                Intent intent = new Intent((AccountFragment.this.getActivity()), GcmRegistrationService.class);
                                (AccountFragment.this.getActivity()).startService(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(AccountFragment.this.getActivity(), "WARNING! Could not refresh ordering settings from website. Order functions may work incorrectly", Toast.LENGTH_LONG).show();
                        }
                        //clear orders in local DB just in case
                        dataModel.clearOrders();
                        //display account info
                        refreshAccountInfo();
                        switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
                    } else {
                        Toast.makeText(AccountFragment.this.getActivity(), "Registration failed. Please try again. Phone and Name should not be empty", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot complete registration", Toast.LENGTH_LONG).show();
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Task to perform customer's info update
     */
    protected class CustomerInfoSaver extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(AccountFragment.this.getActivity());
            progressDialog.setMessage("Saving info...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - saving result (updated customer info)
            //needs customer's new name and email
            //returns http response as JSON object or null
            try {
                String name = (String) objects[0];
                String email = (String) objects[1];
                return dataModel.getSavingInfoResult(name, email);
            } catch (Exception e) {
                e.printStackTrace();
                AccountFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot get response from server", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            try {
                //if got response from server
                if (o != null) {
                    //save account settings
                    boolean infoSaveSuccessful = dataModel.applyCustomerSettings((JSONObject) o);
                    //If successful
                    if (infoSaveSuccessful) {
                        //display account info
                        refreshAccountInfo();
                        switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
                    } else {
                        Toast.makeText(AccountFragment.this.getActivity(), "Saving failed. Please try again. Name should not be empty", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot complete saving", Toast.LENGTH_LONG).show();
            } finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Task to perform customer's password change
     */
    protected class CustomerPasswordChanger extends AsyncTask {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            //show dialog
            progressDialog = new ProgressDialog(AccountFragment.this.getActivity());
            progressDialog.setMessage("Changing password...");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects){
            //get response from web server - password change result
            //needs customer's current password name and new passord
            //returns http response as JSON object or null
            try {
                String oldPassword = (String) objects[0];
                String newPassword = (String) objects[1];
                return dataModel.changePassword(oldPassword, newPassword);
            } catch (Exception e) {
                e.printStackTrace();
                AccountFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot get response from server", Toast.LENGTH_LONG).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            //if got response from server
            if (o != null) {
                boolean success = (Boolean) o;
                if (success) {
                    //display account info
                    refreshAccountInfo();
                    switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
                } else {
                    //show warning if change was unsuccessful
                    AlertDialog alertDialog;
                    alertDialog = new AlertDialog.Builder(AccountFragment.this.getActivity()).create();
                    alertDialog.setTitle("Error!");
                    alertDialog.setMessage("Password was not changed. Try again. Make sure old password is correct");
                    alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CLOSE", (DialogInterface.OnClickListener) null);
                    alertDialog.show();
                }
            }
            progressDialog.dismiss();
        }
    }

    //**********************************************************************************************

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set data model for easy access
        dataModel = ((MainActivity) this.getActivity()).dataModel;

        //log in customer
        btnLoginListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                String phone = ((EditText) AccountFragment.this.getView().findViewById(R.id.editLoginPhone)).getText().toString();
                String password = ((EditText) AccountFragment.this.getView().findViewById(R.id.editLoginPassword)).getText().toString();
                //check if phone is not empty
                if (phone.equals("")) {
                    Toast.makeText(AccountFragment.this.getActivity(), "Phone should not be empty", Toast.LENGTH_LONG).show();
                }
                //log in
                else {
                    CustomerLoginer loginer = new CustomerLoginer();
                    loginer.execute(phone, password);
                }
            }
        };

        //register new customer
        btnRegisterListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                String phone = ((EditText) AccountFragment.this.getView().findViewById(R.id.editRegisterPhone)).getText().toString();
                String name = ((EditText) AccountFragment.this.getView().findViewById(R.id.editRegisterName)).getText().toString();
                String email = ((EditText) AccountFragment.this.getView().findViewById(R.id.editRegisterEmail)).getText().toString();
                String password = ((EditText) AccountFragment.this.getView().findViewById(R.id.editRegisterPassword)).getText().toString();
                String passwordConfirm = ((EditText) AccountFragment.this.getView().findViewById(R.id.editRegisterPasswordConfirm)).getText().toString();
                //check that phone and anme is not empty
                if (phone.equals("") || name.equals("")) {
                    Toast.makeText(AccountFragment.this.getActivity(), "Phone and Name should not be empty", Toast.LENGTH_LONG).show();
                }
                //check if password and confirmation match
                else if (!password.equals(passwordConfirm)) {
                    Toast.makeText(AccountFragment.this.getActivity(), "Password and confirmation do not match", Toast.LENGTH_LONG).show();
                }
                //register customer
                else {
                    CustomerRegistrator registrator = new CustomerRegistrator();
                    registrator.execute(phone, name, email, password);
                }
            }
        };

        //log out
        btnLogoutListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if confirmed log out customer
                AlertDialog alertDialog;
                alertDialog = new AlertDialog.Builder(AccountFragment.this.getActivity()).create();
                alertDialog.setTitle("Confirm");
                alertDialog.setMessage("Do you want to log out?");
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", (DialogInterface.OnClickListener) null);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    //erase GCM id on server
                                    ((MainActivity) AccountFragment.this.getActivity()).unregisterGcm(dataModel.settings.gcmId, dataModel.settings.customerId, dataModel.settings.customerToken);
                                    //erase accout settings
                                    dataModel.clearCustomerSettings();
                                    //erase orders
                                    dataModel.clearOrders();
                                    //clear info page
                                    refreshAccountInfo();
                                    //go to login view
                                    switchLayoutState(LAYOUT_STATE_LOGIN);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(AccountFragment.this.getActivity(), "ERROR! Cannot complete log out. Please restart the application", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                );
                alertDialog.show();
            }
        };

        //refresh account info (check credentials and token validity)
        btnRefreshCustomerInfoListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) AccountFragment.this.getActivity()).refreshCustomerInfo();
            }
        };

        //save (update) customer info
        btnSaveCustomerInfoListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                String name = ((EditText) AccountFragment.this.getView().findViewById(R.id.editNewName)).getText().toString();
                String email = ((EditText) AccountFragment.this.getView().findViewById(R.id.editNewEmail)).getText().toString();
                //check  that name is not empty
                if (name.equals("")) {
                    Toast.makeText(AccountFragment.this.getActivity(), "Name should not be empty", Toast.LENGTH_LONG).show();
                }
                //update info
                else {
                    CustomerInfoSaver saver = new CustomerInfoSaver();
                    saver.execute(name, email);
                }
            }
        };

        //
        btnChangePasswordListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                String oldPassword = ((EditText) AccountFragment.this.getView().findViewById(R.id.editOldPassword)).getText().toString();
                String newPassword = ((EditText) AccountFragment.this.getView().findViewById(R.id.editNewPassword)).getText().toString();
                String newPasswordConfirm = ((EditText) AccountFragment.this.getView().findViewById(R.id.editNewPasswordConfirm)).getText().toString();
                //check if new password and confirmation match
                if (!newPassword.equals(newPasswordConfirm)) {
                    Toast.makeText(AccountFragment.this.getActivity(), "Password and confirmation do not match", Toast.LENGTH_LONG).show();
                }
                //update password
                else {
                    CustomerPasswordChanger customerPasswordChanger = new CustomerPasswordChanger();
                    customerPasswordChanger.execute(oldPassword, newPassword);
                }
            }
        };

        //go register
        btnGoRegisterListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                switchLayoutState(LAYOUT_STATE_REGISTER);
            }
        };

        //go login
        btnGoLoginListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                switchLayoutState(LAYOUT_STATE_LOGIN);
            }
        };

        //go edit info
        btnGoEditListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                switchLayoutState(LAYOUT_STATE_EDIT_INFO);
            }
        };

        //go account info
        btnGoInfoListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                ((MainActivity) AccountFragment.this.getActivity()).hideKeyboard();
                //
                switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //show info
        refreshAccountInfo();
        //if customer is logged in swithc to info view
        if ((dataModel.settings.customerId != 0) && (!dataModel.settings.customerToken.equals(""))) {
            switchLayoutState(LAYOUT_STATE_ACCOUNT_INFO);
        }
        //otherwise to log in view
        else {
            switchLayoutState(LAYOUT_STATE_LOGIN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        //find interface elements
        Button buttonLogin = (Button) view.findViewById(R.id.buttonLogin);
        Button buttonRegister = (Button) view.findViewById(R.id.buttonRegister);
        Button buttonLogout = (Button) view.findViewById(R.id.buttonLogout);
        Button buttonAccountInfoRefresh = (Button) view.findViewById(R.id.buttonAccountInfoRefresh);
        Button buttonNewInfoSave = (Button) view.findViewById(R.id.buttonNewInfoSave);
        Button buttonChangePassword = (Button) view.findViewById(R.id.buttonChangePassword);
        Button buttonGoRegister = (Button) view.findViewById(R.id.buttonGoRegister);
        Button buttonGoLogin = (Button) view.findViewById(R.id.buttonGoLogin);
        Button buttonGoEdit = (Button) view.findViewById(R.id.buttonGoEdit);
        Button buttonGoInfo = (Button) view.findViewById(R.id.buttonGoInfo);

        //set buttons` listeners
        buttonLogin.setOnClickListener(btnLoginListener);
        buttonRegister.setOnClickListener(btnRegisterListener);
        buttonLogout.setOnClickListener(btnLogoutListener);
        buttonAccountInfoRefresh.setOnClickListener(btnRefreshCustomerInfoListener);
        buttonNewInfoSave.setOnClickListener(btnSaveCustomerInfoListener);
        buttonChangePassword.setOnClickListener(btnChangePasswordListener);
        buttonGoRegister.setOnClickListener(btnGoRegisterListener);
        buttonGoLogin.setOnClickListener(btnGoLoginListener);
        buttonGoEdit.setOnClickListener(btnGoEditListener);
        buttonGoInfo.setOnClickListener(btnGoInfoListener);

        return view;
    }

    //**********************************************************************************************

    /**
     * Switch view of the fragment. Hide layouts of the switchable set and show one of them
     * @param state State to switch to
     */
    protected void switchLayoutState(int state) {
        View view = AccountFragment.this.getView();
        LinearLayout layoutLogin = (LinearLayout) view.findViewById(R.id.layoutLogin);
        LinearLayout layoutRegister = (LinearLayout) view.findViewById(R.id.layoutRegister);
        LinearLayout layoutAccountInfo = (LinearLayout) view.findViewById(R.id.layoutAccountInfo);
        LinearLayout layoutEditInfo = (LinearLayout) view.findViewById(R.id.layoutEditInfo);

        layoutLogin.setVisibility(View.GONE);
        layoutRegister.setVisibility(View.GONE);
        layoutAccountInfo.setVisibility(View.GONE);
        layoutEditInfo.setVisibility(View.GONE);

        switch (state){
            case LAYOUT_STATE_LOGIN:
                layoutLogin.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_REGISTER:
                layoutRegister.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_ACCOUNT_INFO:
                layoutAccountInfo.setVisibility(View.VISIBLE);
                break;
            case LAYOUT_STATE_EDIT_INFO:
                layoutEditInfo.setVisibility(View.VISIBLE);
                break;
        }
        AccountFragment.this.layoutState = state;
    }

    /**
     * Refresh account info from app settings
     */
    protected void refreshAccountInfo() {
        //find interface elements
        View view = AccountFragment.this.getView();
        TextView textInfoPhone = (TextView) view.findViewById(R.id.textInfoPhone);
        TextView textInfoName = (TextView) view.findViewById(R.id.textInfoName);
        TextView textInfoEmail = (TextView) view.findViewById(R.id.textInfoEmail);
        TextView textAccountInactive = (TextView) view.findViewById(R.id.textAccountInactive);
        EditText editNewName = (EditText) view.findViewById(R.id.editNewName);
        EditText editNewEmail = (EditText) view.findViewById(R.id.editNewEmail);
        EditText editOldPassword = (EditText) view.findViewById(R.id.editOldPassword);
        EditText editNewPassword = (EditText) view.findViewById(R.id.editNewPassword);
        EditText editNewPasswordConfirm = (EditText) view.findViewById(R.id.editNewPasswordConfirm);
        EditText editLoginPhone = (EditText) view.findViewById(R.id.editLoginPhone);
        EditText editLoginPassword = (EditText) view.findViewById(R.id.editLoginPassword);
        EditText editRegisterPhone = (EditText) view.findViewById(R.id.editRegisterPhone);
        EditText editRegisterName = (EditText) view.findViewById(R.id.editRegisterName);
        EditText editRegisterEmail = (EditText) view.findViewById(R.id.editRegisterEmail);
        EditText editRegisterPassword = (EditText) view.findViewById(R.id.editRegisterPassword);
        EditText editRegisterPasswordConfirm = (EditText) view.findViewById(R.id.editRegisterPasswordConfirm);

        //set text
        textInfoPhone.setText(dataModel.settings.customerPhone);
        textInfoName.setText(dataModel.settings.customerName);
        textInfoEmail.setText(dataModel.settings.customerEmail);
        editNewName.setText(dataModel.settings.customerName);
        editNewEmail.setText(dataModel.settings.customerEmail);
        editOldPassword.setText("");
        editNewPassword.setText("");
        editNewPasswordConfirm.setText("");
        editLoginPhone.setText("");
        editLoginPassword.setText("");
        editRegisterPhone.setText("");
        editRegisterName.setText("");
        editRegisterEmail.setText("");
        editRegisterPassword.setText("");
        editRegisterPasswordConfirm.setText("");

        //show label if account is not active
        if (dataModel.settings.customerActive) {
            textAccountInactive.setVisibility(View.GONE);
        } else {
            textAccountInactive.setVisibility(View.VISIBLE);
        }
    }
}
