package nz.fondou.fondou;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Class to perform operations with app business data. Sen requests to server, process responses, work with local DB
 */
public class DataModel {
    //flags of data synchronization with server
    protected boolean menuSynchronized = false;
    protected boolean ordersSynchronized = false;
    protected boolean credentialsChecked = false;
    //server address
    private static final String API_URL = "http://fondou.u7907063.isp.regruhosting.ru/api/";
    //order statuses for local DB records
    protected static final String ORDER_STATUS_CREATED = "created", ORDER_STATUS_PROCESSED = "processed", ORDER_STATUS_SENT = "sent";

    //**********************************************************************************************

    //database access
    private FondouDatabaseHelper databaseHelper;
    //application settings
    protected Settings settings = new Settings();

    //**********************************************************************************************

    //Application settings
    protected static class Settings{
        //current customer account info
        public int customerId;
        public String customerToken;
        public boolean customerActive;
        public String customerPhone;
        public String customerName;
        public String customerEmail;
        //GCM id for the app
        public String gcmId;
        //timestamps of last synchronization with server
        public String menuTimestamp;
        public String ordersTimestamp;
        //ordering conditions, read from server
        public int minimumTimeFrameSimple;
        public int minimumTimeFrameAdvanced;
        public int orderingStartHour;
        public int orderingStartMinute;
        public int orderingFinishHour;
        public int orderingFinishMinute;
        public int maxNoDepositCostForSimpleOrder;
        public int maxNoDepositCostForAdvancedOrder;

    }

    //Key - Value record for HTTP requests function
    protected static class KeyValue{
        public final String key;
        public final String value;

        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    //Class to store data about the current order (when making new order)
    protected static class Order {
        public Date orderTime = new Date();
        public Date deliveryTime = new Date();
        public long timeFrame;
        public boolean advanced;
        public String address = "NONE";
        public BigDecimal totalPrice = new BigDecimal(0);
        public ArrayList<OrderItem> items = new ArrayList<>();

        //Class to store data about current order item
        protected static class OrderItem {

            public final int id;
            public final int count;
            public final String name;
            public final BigDecimal basePrice;
            public final BigDecimal totalPrice;
            public final ArrayList<ItemOption> options;

            //Class to store data about current order option
            public static class ItemOption{
                public final int optionId;
                public final String optionName;
                public final int valueId;
                public final String value;
                public final BigDecimal priceModifier;

                public ItemOption(int optionId, String optionName, int valueId, String value, BigDecimal priceModifier) {
                    this.optionId = optionId;
                    this.optionName = optionName;
                    this.valueId = valueId;
                    this.value = value;
                    this.priceModifier = priceModifier;
                }
            }

            public OrderItem(int id, int count, String name, BigDecimal basePrice, ArrayList<ItemOption> options) {
                this.id = id;
                this.count = count;
                this.name = name;
                this.basePrice = basePrice;
                this.options = options;
                BigDecimal totalPrice = this.basePrice;
                for (int i = 0; i < options.size(); i++) {
                    totalPrice = totalPrice.add(options.get(i).priceModifier);
                }
                this.totalPrice = totalPrice;
            }
        }

        public Order(boolean advanced) {
            this.advanced = advanced;
        }
    }

    //Class to store data about posted order
    protected static class PostedOrder {
        //Class to store data about posted order item
        protected static class PostedOrderItem {
            public final int id;
            public final int count;
            public final String name;
            public final String description;
            public final BigDecimal price;

            public PostedOrderItem(int id, int count, String name, String description, BigDecimal price) {
                this.id = id;
                this.count = count;
                this.name = name;
                this.description = description;
                this.price = price;
            }
        }

        public final int id;
        public final Date orderTime;
        public final Date deliveryTime;
        public final boolean advanced;
        public final String address;
        public final BigDecimal totalPrice;
        public final ArrayList<PostedOrderItem> items;

        public PostedOrder(int id, Date orderTime, Date deliveryTime, boolean advanced, String address, BigDecimal totalPrice, ArrayList<PostedOrderItem> items) {
            this.id = id;
            this.orderTime = orderTime;
            this.deliveryTime = deliveryTime;
            this.advanced = advanced;
            this.address = address;
            this.totalPrice = totalPrice;
            this.items = items;
        }
    }

    //Class to store data about menu category
    protected static class MenuCategory {
        public final int id;
        public final String name;
        public final ArrayList<MenuItem> items;
        public final ArrayList<Option> options;

        //Class to store data about menu item
        protected static class MenuItem {
            public final int id;
            public final String name;
            public final BigDecimal basePrice;

            public MenuItem(int id, String name, BigDecimal basePrice) {
                this.id = id;
                this.name = name;
                this.basePrice = basePrice;
            }
        }

        public MenuCategory(int id, String name, ArrayList<MenuItem> items, ArrayList<Option> options) {
            this.id = id;
            this.name = name;
            this.items = items;
            this.options = options;
        }
    }

    //Class to store data about category option
    protected static class Option {
        public final int id;
        public final String name;
        public final ArrayList<OptionValue> values;
        public int currentValuePosition = 0;

        //Class to store data about option value
        protected static class OptionValue {
            public final int id;
            public final String value;
            public final BigDecimal priceModifier;

            public OptionValue(int id, String value, BigDecimal priceModifier) {
                this.id = id;
                this.value = value;
                this.priceModifier = priceModifier;
            }
        }

        public Option(int id, String name, ArrayList<OptionValue> values) {
            this.id = id;
            this.name = name;
            this.values = values;
        }
    }

    //Class to store data about menu item being added to current order at the moment
    protected static class CurrentMenuItem extends MenuCategory.MenuItem {
        public int count = 1;
        public final ArrayList<Option> options;

        public CurrentMenuItem(MenuCategory.MenuItem menuItem, MenuCategory menuCategory) {
            super(menuItem.id, menuItem.name, menuItem.basePrice);
            this.options = menuCategory.options;
        }
    }

    //**********************************************************************************************

    //Exception to throw if HTTP response code indicates error
    protected static class ServerAPIException extends Exception{
        int httpErrorCode;
        String httpResponse;
        public ServerAPIException(String message, int httpErrorCode, String httpResponse){
            super(message);
            this.httpErrorCode = httpErrorCode;
            this.httpResponse = httpResponse;
        }
    }

    //**********************************************************************************************

    public DataModel(FondouDatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    //**********************************************************************************************

    /**
     * Get menu from local DB
     * @param advancedOrder Flag to choose order type, selected menu categories depend on it
     * @return List of menu categories with items and options for each
     * @throws Exception
     */
    public ArrayList<MenuCategory> getMenu(boolean advancedOrder) throws Exception {
        ArrayList<MenuCategory> menu = new ArrayList<>();
        SQLiteDatabase connection = databaseHelper.getConnection();
        //read menu categories of chosen type
        int orderingType = advancedOrder ? FondouDatabaseHelper.ORDERING_ADVANCED : FondouDatabaseHelper.ORDERING_SIMPLE;
        ArrayList<FondouDatabaseHelper.MenuCategoryRec> categoriesList = databaseHelper.getMenuCategoriesList(connection, orderingType);
        //for each category
        for (int i = 0; i < categoriesList.size(); i++) {
            //read items
            ArrayList<MenuCategory.MenuItem> items = new ArrayList<>();
            ArrayList<FondouDatabaseHelper.MenuItemRec> itemsList = databaseHelper.getCategoryItemsList(connection, categoriesList.get(i).id);
            for (int j = 0; j < itemsList.size(); j++) {
                String name = itemsList.get(j).name;
                if (itemsList.get(j).size.length() > 0) {
                    name = name + " " + itemsList.get(j).size;
                }
                BigDecimal price = new BigDecimal(itemsList.get(j).price).divide(new BigDecimal(100));
                items.add(new MenuCategory.MenuItem(itemsList.get(j).id, name, price));
            }
            //read options
            ArrayList<Option> options = new ArrayList<>();
            ArrayList<FondouDatabaseHelper.OptionRec> optionsList = databaseHelper.getCategoryOptionsList(connection, categoriesList.get(i).id);
            //for each option
            for (int j = 0; j < optionsList.size(); j++) {
                //read option values
                ArrayList<Option.OptionValue> values = new ArrayList<>();
                ArrayList<FondouDatabaseHelper.OptionValueRec> valuesList = databaseHelper.getOptionValuesList(connection, optionsList.get(j).id);
                for (int k = 0; k < valuesList.size(); k++) {
                    BigDecimal priceModifier = new BigDecimal(valuesList.get(k).priceModifier).divide(new BigDecimal(100));
                    values.add(new Option.OptionValue(valuesList.get(k).id, valuesList.get(k).value, priceModifier));
                }
                //
                options.add(new Option(optionsList.get(j).id, optionsList.get(j).name, values));
            }
            //
            menu.add(new MenuCategory(categoriesList.get(i).id, categoriesList.get(i).name, items, options));
        }
        databaseHelper.closeConnection(connection);

        return menu;
    }

    /**
     * Get list of orders from local DB
     * @param orderStatus Status of orders to select
     * @return List of orders with items
     * @throws Exception
     */
    public ArrayList<PostedOrder> getOrdersList(String orderStatus) throws Exception {
        ArrayList<PostedOrder> orders = new ArrayList<>();
        SQLiteDatabase connection = databaseHelper.getConnection();
        //read orders
        ArrayList<FondouDatabaseHelper.OrderRec> orderList = databaseHelper.getOrdersList(connection, orderStatus);
        //for each order
        for (int i = 0; i < orderList.size(); i++) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            int orderId = orderList.get(i).id;
            Date orderTime = format.parse(orderList.get(i).orderTime);
            Date deliveryTime = format.parse(orderList.get(i).deliveryTime);
            boolean advanced = orderList.get(i).advanced;
            String address = orderList.get(i).address;
            BigDecimal totalPrice = new BigDecimal(orderList.get(i).totalPrice).divide(new BigDecimal(100));
            //read items
            ArrayList<PostedOrder.PostedOrderItem> items = new ArrayList<>();
            ArrayList<FondouDatabaseHelper.OrderItemRec> itemsList = databaseHelper.getOrderItemsList(connection, orderList.get(i).id);
            for (int j = 0; j < itemsList.size(); j++) {
                int itemId = itemsList.get(j).id;
                int count = itemsList.get(j).count;
                String name = itemsList.get(j).name;
                String description = itemsList.get(j).description;
                BigDecimal price = new BigDecimal(itemsList.get(j).price).divide(new BigDecimal(100));
                items.add(new PostedOrder.PostedOrderItem(itemId, count, name, description, price));
            }
            //
            orders.add(new PostedOrder(orderId, orderTime, deliveryTime, advanced, address, totalPrice, items));
        }
        databaseHelper.closeConnection(connection);

        return orders;
    }

    //**********************************************************************************************

    /**
     * Send HTTP request and return response
     * @param address Server address
     * @param method request method: get, post, put, delete
     * @param parameters list of request parameters
     * @return JSON: server api must always reply with encoded JSON object
     * @throws Exception
     */
    private JSONObject httpRequest(String address, String method, ArrayList<KeyValue> parameters) throws Exception{
        //create parameters string
        String paramString = "";
        if (parameters != null) {
            if (parameters.size() > 0) {
                paramString = URLEncoder.encode(parameters.get(0).key, "UTF-8") + "=" + URLEncoder.encode(parameters.get(0).value, "UTF-8");
                for (int i = 1; i < parameters.size(); i++) {
                    paramString = paramString + "&" + URLEncoder.encode(parameters.get(i).key, "UTF-8") + "=" + URLEncoder.encode(parameters.get(i).value, "UTF-8");
                }
            }
        }

        //prepare request
        URL url;
        HttpURLConnection urlConnection;
        switch (method) {
            case "GET":
            case "DELETE":
                url = new URL(API_URL + address + "?" + paramString);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod(method);
                break;
            case "POST":
            case "PUT":
                url = new URL(API_URL + address);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod(method);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded" );
                urlConnection.setRequestProperty("Content-Length", "" + Integer.toString(paramString.getBytes().length));
                OutputStream outputStream = urlConnection.getOutputStream();
                outputStream.write(paramString.getBytes("UTF-8"));
                break;
            default:
                return null;
        }
        //
        Log.i("HTTP REQUEST", "method = " + method);
        Log.i("HTTP REQUEST", "url = " + url.toString());
        Log.i("HTTP REQUEST", "params = " + paramString);

        try {
            //get response
            urlConnection.connect();
            int responseCode = urlConnection.getResponseCode();
            //
            Log.i("HTTP RESULT", "code = " + responseCode);

            //if HTTP error
            if (responseCode != 200) {
                throw new ServerAPIException("API server returns error", responseCode, "");
            }

            //prepare result as JSON object
            JSONObject result = null;
            InputStream inputStream = urlConnection.getInputStream();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            Log.i("HTTP RESULT", "response = " + responseStrBuilder.toString());
            result = new JSONObject(responseStrBuilder.toString());
            return result;
        }
        finally {
            urlConnection.disconnect();
        }
    }

    //**********************************************************************************************

    /**
     * Send log in request and get response obj
     * @param phone Customer's phone num
     * @param password Customer's password
     * @return Log in attempt result
     * @throws Exception
     */
    protected JSONObject getLogInResult(String phone, String password) throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("phone", phone));
        parameters.add(new DataModel.KeyValue("password", password));
        return httpRequest("customers/none/login", "GET", parameters);
    }

    /**
     * Send request to check if phone number and email are not registered yet on server. Process result
     * @param phone Customer's phone num
     * @param email Customer's email
     * @return True if not registered else otherwise
     * @throws Exception
     */
    protected boolean checkPhoneEmailFree(String phone, String email) throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("phone", phone));
        parameters.add(new DataModel.KeyValue("email", email));
        JSONObject result = httpRequest("customers/none/phone-email-check", "GET", parameters);

        String answer = result.getString("result");
        if (answer.equals("NO")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send registration request and get response obj
     * May return error if phone num or email is already registered or if phone num or name is empty
     * @param phone Customer's phone num
     * @param name Customer's name
     * @param email Customer's email
     * @param password Customer's password
     * @return Registration attempt result
     * @throws Exception
     */
    protected JSONObject getRegistrationResult(String phone, String name, String email, String password) throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("phone", phone));
        parameters.add(new DataModel.KeyValue("name", name));
        parameters.add(new DataModel.KeyValue("email", email));
        parameters.add(new DataModel.KeyValue("password", password));
        return httpRequest("customers", "POST", parameters);
    }

    /**
     * Send save customer info request and get result obj. Credentials check needed
     * @param name Customer's name
     * @param email Customer's email
     * @return Saving attempt result
     * @throws Exception
     */
    protected JSONObject getSavingInfoResult(String name, String email) throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("name", name));
        parameters.add(new DataModel.KeyValue("email", email));
        parameters.add(new DataModel.KeyValue("customer_token", settings.customerToken));
        return httpRequest("customers/" + settings.customerId + "/info", "PUT", parameters);
    }

    /**
     * Save account settings to app settings object and to DB
     * @param loginRec Server response obj
     * @return True if authorization was successful, false if not
     * @throws Exception
     */
    protected boolean applyCustomerSettings(JSONObject loginRec) throws Exception{
        String result = loginRec.getString("result");
        if (result.equals("SUCCESS")) {
            SQLiteDatabase connection = databaseHelper.getConnection();

            FondouDatabaseHelper.SettingsRec record;
            DataModel.this.settings.customerId = loginRec.getInt("id");
            record = new FondouDatabaseHelper.SettingsRec("CustomerId", String.valueOf(settings.customerId));
            databaseHelper.updateSetting(connection, record);
            DataModel.this.settings.customerActive = loginRec.getBoolean("active");
            record = new FondouDatabaseHelper.SettingsRec("CustomerActive", String.valueOf(settings.customerActive));
            databaseHelper.updateSetting(connection, record);
            DataModel.this.settings.customerToken = loginRec.getString("accessToken");
            record = new FondouDatabaseHelper.SettingsRec("CustomerToken", settings.customerToken);
            databaseHelper.updateSetting(connection, record);
            DataModel.this.settings.customerPhone = loginRec.getString("phone");
            record = new FondouDatabaseHelper.SettingsRec("CustomerPhone", settings.customerPhone);
            databaseHelper.updateSetting(connection, record);
            DataModel.this.settings.customerName = loginRec.getString("name");
            record = new FondouDatabaseHelper.SettingsRec("CustomerName", settings.customerName);
            databaseHelper.updateSetting(connection, record);
            DataModel.this.settings.customerEmail = loginRec.getString("email");
            record = new FondouDatabaseHelper.SettingsRec("CustomerEmail", settings.customerEmail);
            databaseHelper.updateSetting(connection, record);

            databaseHelper.closeConnection(connection);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Send token (credentials) check result and get response
     * @param customerId Customer's record id
     * @param customerToken Customer's token
     * @return Response obj
     * @throws Exception
     */
    protected JSONObject getTokenCheckResult(int customerId, String customerToken) throws Exception {
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("id", String.valueOf(customerId)));
        parameters.add(new DataModel.KeyValue("token", customerToken));
        return httpRequest("customers/" + settings.customerId + "/token", "GET", parameters);
    }

    /**
     * Process results of customer credentials check. Save new settings to local DB
     * @param result Server response
     * @throws Exception
     */
    protected String[] processTokenCheckResult(JSONObject result) throws Exception {
        String answer = result.getString("result");
        switch (answer) {
            //Everything is OK
            case "OK":
                settings.customerActive = true;
                saveSetting("CustomerActive", "true");
                return new String[] {"OK"};
            //Customer account not activated (yet)
            case "INACTIVE":
                settings.customerActive = false;
                saveSetting("CustomerActive", "false");
                return new String[] {"INACTIVE"};
            //token is invalid
            case "FAIL":
                String[] returnVal = new String[]{"FAIL", settings.gcmId, String.valueOf(settings.customerId), settings.customerToken};
                //erase customer info
                clearCustomerSettings();
                //erase orders from local DB
                clearOrders();
                return returnVal;
            default:
                return new String[]{};
        }
    }

    /**
     * Send password change request and process response.
     * @param oldPassword Current customer's password
     * @param newPassword New customer's password
     * @return True if successful false otherwise
     * @throws Exception
     */
    protected boolean changePassword(String oldPassword, String newPassword) throws Exception {
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("old_password", oldPassword));
        parameters.add(new DataModel.KeyValue("new_password", newPassword));
        JSONObject response = httpRequest("customers/" + settings.customerId + "/password", "PUT", parameters);

        String answer = response.getString("result");
        if (answer.equals("SUCCESS")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Send register GCM id request, save setting
     * @param gcmId GCM id of the app
     * @throws Exception
     */
    protected void registerGcmId(String gcmId) throws Exception {
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("gcm_id", gcmId));
        parameters.add(new DataModel.KeyValue("customer_token", settings.customerToken));
        httpRequest("customers/" + settings.customerId + "/gcm-id", "PUT", parameters);

        settings.gcmId = gcmId;
        saveSetting("GCMid", gcmId);
    }

    /**
     * Send delete GCM id request, save setting
     * @param gcmId GCM id of the app
     * @throws Exception
     */
    protected void unregisterGcmId(String gcmId, int customerId, String customerToken) throws Exception {
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("gcm_id", gcmId));
        parameters.add(new DataModel.KeyValue("customer_token", customerToken));
        httpRequest("customers/" + customerId + "/gcm-id-clear", "PUT", parameters);
        settings.gcmId = "";
        saveSetting("GCMid", "");
    }

    //**********************************************************************************************

    /**
     * Get menu changes from server (after previous synchronization)
     * @return Menu changes from server
     * @throws Exception
     */
    protected JSONObject getMenuFromServer() throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("timestamp", DataModel.this.settings.menuTimestamp));
        return httpRequest("menu", "GET", parameters);
    }

    /**
     * Process menu changes object and save changes to local DB
     * @param serverMenu Changes obj from server
     * @throws Exception
     */
    protected void saveMenuToDB(JSONObject serverMenu) throws Exception{
        SQLiteDatabase connection = databaseHelper.getConnection();

        //delete outdated categories
        JSONArray categoryIds = serverMenu.getJSONArray("categoryIds");
        ArrayList<Integer> categoryList = new ArrayList<>();
        for (int i = 0; i < categoryIds.length(); i++) {
            categoryList.add(categoryIds.getInt(i));
        }
        databaseHelper.deleteMenuCategoriesNotInList(connection, categoryList);
        //synchronize menu categories
        JSONArray categories = serverMenu.getJSONArray("categories");
        for (int i = 0; i < categories.length(); i++) {
            JSONObject category = categories.getJSONObject(i);
            int id = category.getInt("id");
            String name = category.getString("name");
            boolean simpleOrdering = category.getBoolean("orderingSimple");
            boolean advancedOrdering = category.getBoolean("orderingAdvanced");

            FondouDatabaseHelper.MenuCategoryRec record = new FondouDatabaseHelper.MenuCategoryRec(id, name, simpleOrdering, advancedOrdering);
            if (databaseHelper.isMenuCategoryExists(connection, id)){
                databaseHelper.updateMenuCategory(connection, record);
            } else {
                databaseHelper.addMenuCategory(connection, record);
            }
        }

        //delete outdated items
        JSONArray itemIds = serverMenu.getJSONArray("itemIds");
        ArrayList<Integer> itemList = new ArrayList<>();
        for (int i = 0; i < itemIds.length(); i++) {
            itemList.add(itemIds.getInt(i));
        }
        databaseHelper.deleteMenuItemsNotInList(connection, itemList);
        //synchronize menu items
        JSONArray items = serverMenu.getJSONArray("items");
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);
            int id = item.getInt("id");
            int categoryId = item.getInt("categoryId");
            int sizeNum = item.getInt("sizeNum");
            String name = item.getString("name");
            String size = item.getString("size");
            String description = item.getString("description");
            int price = item.getInt("price");
            boolean active = item.getBoolean("active");

            FondouDatabaseHelper.MenuItemRec record = new FondouDatabaseHelper.MenuItemRec(id, categoryId, name, size, description, sizeNum, price, active);
            if (databaseHelper.isMenuItemExists(connection, id)){
                databaseHelper.updateMenuItem(connection, record);
            } else {
                databaseHelper.addMenuItem(connection, record);
            }
        }

        //delete outdated options
        JSONArray optionIds = serverMenu.getJSONArray("optionIds");
        ArrayList<Integer> optionList = new ArrayList<>();
        for (int i = 0; i < optionIds.length(); i++) {
            optionList.add(optionIds.getInt(i));
        }
        databaseHelper.deleteOptionsNotInList(connection, optionList);
        //synchronize options
        JSONArray options = serverMenu.getJSONArray("options");
        for (int i = 0; i < options.length(); i++) {
            JSONObject option = options.getJSONObject(i);
            int id = option.getInt("id");
            String name = option.getString("name");

            FondouDatabaseHelper.OptionRec record = new FondouDatabaseHelper.OptionRec(id, name);
            if (databaseHelper.isOptionExists(connection, id)){
                databaseHelper.updateOption(connection, record);
            } else {
                databaseHelper.addOption(connection, record);
            }
        }

        //delete outdated category options
        JSONArray categoryOptionIds = serverMenu.getJSONArray("categoryOptionIds");
        ArrayList<Integer> categoryOptionList = new ArrayList<>();
        for (int i = 0; i < categoryOptionIds.length(); i++) {
            categoryOptionList.add(categoryOptionIds.getInt(i));
        }
        databaseHelper.deleteCategoryOptionsNotInList(connection, categoryOptionList);
        //synchronize category options
        JSONArray categoryOptions = serverMenu.getJSONArray("categoryOptions");
        for (int i = 0; i < categoryOptions.length(); i++) {
            JSONObject categoryOption = categoryOptions.getJSONObject(i);
            int id = categoryOption.getInt("id");
            int categoryId = categoryOption.getInt("categoryId");
            int optionId = categoryOption.getInt("optionId");

            FondouDatabaseHelper.CategoryOptionRec record = new FondouDatabaseHelper.CategoryOptionRec(id, categoryId, optionId);
            if (databaseHelper.isCategoryOptionExists(connection, id)){
                databaseHelper.updateCategoryOption(connection, record);
            } else {
                databaseHelper.addCategoryOption(connection, record);
            }
        }

        //delete outdated option values
        JSONArray optionValueIds = serverMenu.getJSONArray("optionValueIds");
        ArrayList<Integer> optionValueList = new ArrayList<>();
        for (int i = 0; i < optionValueIds.length(); i++) {
            optionValueList.add(optionValueIds.getInt(i));
        }
        databaseHelper.deleteOptionValuesNotInList(connection, optionValueList);
        //synchronize option values
        JSONArray optionValues = serverMenu.getJSONArray("optionValues");
        for (int i = 0; i < optionValues.length(); i++) {
            JSONObject optionValue = optionValues.getJSONObject(i);
            int id = optionValue.getInt("id");
            int optionId = optionValue.getInt("optionId");
            int valueOrder = optionValue.getInt("order");
            String value = optionValue.getString("value");
            int priceModifier = optionValue.getInt("priceModifier");

            FondouDatabaseHelper.OptionValueRec record = new FondouDatabaseHelper.OptionValueRec(id, optionId, valueOrder, value, priceModifier);
            if (databaseHelper.isOptionValueExists(connection, id)){
                databaseHelper.updateOptionValue(connection, record);
            } else {
                databaseHelper.addOptionValue(connection, record);
            }
        }

        //renew timestamp of last synchronization
        DataModel.this.settings.menuTimestamp = serverMenu.getString("timestamp");
        FondouDatabaseHelper.SettingsRec record = new FondouDatabaseHelper.SettingsRec("MenuTimestamp", DataModel.this.settings.menuTimestamp);
        databaseHelper.updateSetting(connection, record);
        //
        databaseHelper.closeConnection(connection);
        //flag
        DataModel.this.menuSynchronized = true;
    }

    //**********************************************************************************************

    /**
     * Send post order request to server and get response as new (posted) order JSON object. Credentials check needed
     * Returned order may differ from posted one (price, items set) depending on how server process the request. It is likely to happen if menu is not synchronized
     * May return error if ordering conditions are broken (timing, items set)
     * @param order Current new order
     * @return Actual new order
     * @throws Exception
     */
    protected JSONObject postOrderToServer(Order order) throws Exception{
        //create JSON object for request from order onject
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JSONObject jsonOrder = new JSONObject();
        jsonOrder.put("advanced", order.advanced);
        jsonOrder.put("orderTime", format.format(order.orderTime));
        jsonOrder.put("deliveryTime", format.format(order.deliveryTime));
        jsonOrder.put("timeFrame", order.timeFrame);
        jsonOrder.put("address", order.address);
        //items
        JSONArray jsonItems = new JSONArray();
        for (int i = 0; i < order.items.size(); i++) {
            Order.OrderItem item = order.items.get(i);
            JSONObject jsonItem = new JSONObject();
            jsonItem.put("id", item.id);
            jsonItem.put("count", item.count);
            //options
            JSONArray jsonOptions= new JSONArray();
            for (int j = 0; j < item.options.size(); j++) {
                Order.OrderItem.ItemOption option = item.options.get(j);
                JSONObject jsonOption = new JSONObject();
                jsonOption.put("optionId", option.optionId);
                jsonOption.put("valueId", option.valueId);
                jsonOptions.put(jsonOption);
            }
            jsonItem.put("options", jsonOptions);
            jsonItems.put(jsonItem);
        }
        jsonOrder.put("items", jsonItems);
        //encode
        String orderString = jsonOrder.toString();

        //HTTP request
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("customer_id", String.valueOf(DataModel.this.settings.customerId)));
        parameters.add(new DataModel.KeyValue("customer_token", DataModel.this.settings.customerToken));
        parameters.add(new DataModel.KeyValue("order", orderString));
        return httpRequest("orders", "POST", parameters);
    }

    /**
     * Process new order object and save it to local DB
     * @param newOrder Order obj from server
     * @throws Exception
     */
    protected void saveNewOrderToDB(JSONObject newOrder) throws Exception{
        SQLiteDatabase connection = databaseHelper.getConnection();

        //synchronize (add new) order
        int orderId = newOrder.getInt("id");
        String orderTime = newOrder.getString("orderTime");
        String deliveryTime = newOrder.getString("deliveryTime");
        String address = newOrder.getString("address");
        boolean advanced = newOrder.getBoolean("advanced");
        String status = newOrder.getString("status");
        int totalPrice = newOrder.getInt("totalPrice");

        FondouDatabaseHelper.OrderRec orderRecord = new FondouDatabaseHelper.OrderRec(orderId, orderTime, deliveryTime, address, advanced, status, totalPrice);
        if (databaseHelper.isOrderExists(connection, orderId)){
            databaseHelper.updateOrder(connection, orderRecord);
        } else {
            databaseHelper.addOrder(connection, orderRecord);
        }

        //synchronize (add new) order items
        JSONArray items = newOrder.getJSONArray("items");
        for (int j = 0; j < items.length(); j++) {
            JSONObject item = items.getJSONObject(j);
            int itemId = item.getInt("id");
            int count = item.getInt("count");
            String name = item.getString("name");
            String description = item.getString("description");
            int price = item.getInt("price");

            FondouDatabaseHelper.OrderItemRec itemRecord = new FondouDatabaseHelper.OrderItemRec(itemId, orderId, count, name, description, price);
            if (databaseHelper.isOrderItemExists(connection, itemId)){
                databaseHelper.updateOrderItem(connection, itemRecord);
            } else {
                databaseHelper.addOrderItem(connection, itemRecord);
            }
        }

        databaseHelper.closeConnection(connection);
    }

    /**
     * Get current customer's orders changes from server (after previous synchronization). Credentials check needed
     * @return Orders changes from server
     * @throws Exception
     */
    protected JSONObject getOrdersFromServer() throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("customer_id", String.valueOf(DataModel.this.settings.customerId)));
        parameters.add(new DataModel.KeyValue("customer_token", DataModel.this.settings.customerToken));
        parameters.add(new DataModel.KeyValue("timestamp", DataModel.this.settings.ordersTimestamp));

        return httpRequest("orders", "GET", parameters);
    }

    /**
     * Process orders changes object and save changes to local DB
     * @param serverOrders Changes obj from server
     * @throws Exception
     */
    protected void saveOrdersToDB(JSONObject serverOrders) throws Exception{
        SQLiteDatabase connection = databaseHelper.getConnection();

        //delete outdated orders
        JSONArray orderIds = serverOrders.getJSONArray("orderIds");
        ArrayList<Integer> categoryList = new ArrayList<>();
        for (int i = 0; i < orderIds.length(); i++) {
            categoryList.add(orderIds.getInt(i));
        }
        databaseHelper.deleteOrdersNotInList(connection, categoryList);

        //delete outdated order items
        JSONArray itemIds = serverOrders.getJSONArray("itemIds");
        ArrayList<Integer> itemList = new ArrayList<>();
        for (int i = 0; i < itemIds.length(); i++) {
            itemList.add(itemIds.getInt(i));
        }
        databaseHelper.deleteOrderItemsNotInList(connection, itemList);

        //synchronize orders
        JSONArray orders = serverOrders.getJSONArray("orders");
        for (int i = 0; i < orders.length(); i++) {
            JSONObject order = orders.getJSONObject(i);
            int orderId = order.getInt("id");
            String orderTime = order.getString("orderTime");
            String deliveryTime = order.getString("deliveryTime");
            String address = order.getString("address");
            boolean advanced = order.getBoolean("advanced");
            String status = order.getString("status");
            int totalPrice = order.getInt("totalPrice");

            FondouDatabaseHelper.OrderRec orderRecord = new FondouDatabaseHelper.OrderRec(orderId, orderTime, deliveryTime, address, advanced, status, totalPrice);
            if (databaseHelper.isOrderExists(connection, orderId)){
                databaseHelper.updateOrder(connection, orderRecord);
            } else {
                databaseHelper.addOrder(connection, orderRecord);
            }

            //synchronize order items
            JSONArray items = order.getJSONArray("items");
            for (int j = 0; j < items.length(); j++) {
                JSONObject item = items.getJSONObject(j);
                int itemId = item.getInt("id");
                int count = item.getInt("count");
                String name = item.getString("name");
                String description = item.getString("description");
                int price = item.getInt("price");

                FondouDatabaseHelper.OrderItemRec itemRecord = new FondouDatabaseHelper.OrderItemRec(itemId, orderId, count, name, description, price);
                if (databaseHelper.isOrderItemExists(connection, itemId)){
                    databaseHelper.updateOrderItem(connection, itemRecord);
                } else {
                    databaseHelper.addOrderItem(connection, itemRecord);
                }
            }
        }

        //renew timestamp of last synchronization
        DataModel.this.settings.ordersTimestamp = serverOrders.getString("timestamp");
        FondouDatabaseHelper.SettingsRec record = new FondouDatabaseHelper.SettingsRec("OrdersTimestamp", DataModel.this.settings.ordersTimestamp);
        databaseHelper.updateSetting(connection, record);
        //
        databaseHelper.closeConnection(connection);
        //flag
        DataModel.this.ordersSynchronized = true;
    }

    /**
     * Send cancel order request and get response. Credentials check needed
     * @param orderId Id or the order record
     * @return Response obj
     * @throws Exception
     */
    protected JSONObject cancelOrderOnServer(int orderId) throws Exception{
        ArrayList<DataModel.KeyValue> parameters = new ArrayList<>();
        parameters.add(new DataModel.KeyValue("customer_id", String.valueOf(DataModel.this.settings.customerId)));
        parameters.add(new DataModel.KeyValue("customer_token", DataModel.this.settings.customerToken));
        return httpRequest("orders/" + orderId + "/cancel", "PUT", parameters);
    }

    /**
     * Process cancel order result and delete it from local DB if it was successful
     * @param cancelResult Result obj
     * @return True if was successful
     * @throws Exception
     */
    protected boolean processCancelledOrder(JSONObject cancelResult) throws Exception {
        int orderId = cancelResult.getInt("orderId");
        String result = cancelResult.getString("result");
        if (result.equals("SUCCESS")) {
            SQLiteDatabase connection = databaseHelper.getConnection();
            databaseHelper.deleteOrderWithItems(connection, orderId);
            databaseHelper.closeConnection(connection);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete all customer's orders from local DB
     * @throws Exception
     */
    protected void clearOrders() throws Exception {
        //reset flag
        ordersSynchronized = false;

        SQLiteDatabase connection = databaseHelper.getConnection();

        //delete records
        databaseHelper.deleteAllOrders(connection);
        databaseHelper.deleteAllOrderItems(connection);
        //reset synchronization timestamp
        settings.ordersTimestamp = "1970-01-01 00:00:00";
        FondouDatabaseHelper.SettingsRec record = new FondouDatabaseHelper.SettingsRec("OrdersTimestamp", settings.ordersTimestamp);
        databaseHelper.updateSetting(connection, record);

        databaseHelper.closeConnection(connection);
    }

    //**********************************************************************************************

    /**
     * Read app settings from local DB and save them to settings object
     * @throws Exception
     */
    protected void readSettings() throws Exception {
        SQLiteDatabase connection = databaseHelper.getConnection();

        ArrayList<FondouDatabaseHelper.SettingsRec> settingsList = databaseHelper.getSettingsList(connection);
        for (int i = 0; i < settingsList.size(); i++) {
            switch (settingsList.get(i).name) {
                case "CustomerId":
                    try {
                        DataModel.this.settings.customerId = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "CustomerToken":
                    DataModel.this.settings.customerToken = settingsList.get(i).value;
                    break;
                case "CustomerActive":
                    try {
                        DataModel.this.settings.customerActive = Boolean.valueOf(settingsList.get(i).value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "CustomerPhone":
                    DataModel.this.settings.customerPhone = settingsList.get(i).value;
                    break;
                case "CustomerName":
                    DataModel.this.settings.customerName = settingsList.get(i).value;
                    break;
                case "CustomerEmail":
                    DataModel.this.settings.customerEmail = settingsList.get(i).value;
                    break;
                case "GCMid":
                    DataModel.this.settings.gcmId = settingsList.get(i).value;
                    break;
                case "MenuTimestamp":
                    DataModel.this.settings.menuTimestamp = settingsList.get(i).value;
                    break;
                case "OrdersTimestamp":
                    DataModel.this.settings.ordersTimestamp = settingsList.get(i).value;
                    break;
                case "MinimumTimeFrameSimple":
                    try {
                        DataModel.this.settings.minimumTimeFrameSimple = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "MinimumTimeFrameAdvanced":
                    try {
                        DataModel.this.settings.minimumTimeFrameAdvanced = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "OrderingStartHour":
                    try {
                        DataModel.this.settings.orderingStartHour = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "OrderingStartMinute":
                    try {
                        DataModel.this.settings.orderingStartMinute = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "OrderingFinishHour":
                    try {
                        DataModel.this.settings.orderingFinishHour = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "OrderingFinishMinute":
                    try {
                        DataModel.this.settings.orderingFinishMinute = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "MaxNoDepositCostForSimpleOrder":
                    try {
                        DataModel.this.settings.maxNoDepositCostForSimpleOrder = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
                case "MaxNoDepositCostForAdvancedOrder":
                    try {
                        DataModel.this.settings.maxNoDepositCostForAdvancedOrder = Integer.valueOf(settingsList.get(i).value);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }

        databaseHelper.closeConnection(connection);
    }

    /**
     * Save particular setting value to local DB
     * @param name Setting name
     * @param value Setting value
     * @throws Exception
     */
    protected void saveSetting(String name, String value) throws Exception {
        SQLiteDatabase connection = databaseHelper.getConnection();

        FondouDatabaseHelper.SettingsRec record = new FondouDatabaseHelper.SettingsRec(name, value);
        databaseHelper.updateSetting(connection, record);

        databaseHelper.closeConnection(connection);
    }

    /**
     * Clear customer account settings from settins obj and local DB
     * @throws Exception
     */
    protected void clearCustomerSettings() throws Exception {
        //object
        DataModel.this.settings.customerId = 0;
        DataModel.this.settings.customerToken = "";
        DataModel.this.settings.customerActive = false;
        DataModel.this.settings.customerPhone = "";
        DataModel.this.settings.customerName = "";
        DataModel.this.settings.customerEmail = "";

        //DB
        SQLiteDatabase connection = databaseHelper.getConnection();

        FondouDatabaseHelper.SettingsRec record;
        record = new FondouDatabaseHelper.SettingsRec("CustomerId", "");
        databaseHelper.updateSetting(connection, record);
        record = new FondouDatabaseHelper.SettingsRec("CustomerToken", "");
        databaseHelper.updateSetting(connection, record);
        record = new FondouDatabaseHelper.SettingsRec("CustomerActive", "");
        databaseHelper.updateSetting(connection, record);
        record = new FondouDatabaseHelper.SettingsRec("CustomerPhone", "");
        databaseHelper.updateSetting(connection, record);
        record = new FondouDatabaseHelper.SettingsRec("CustomerName", "");
        databaseHelper.updateSetting(connection, record);
        record = new FondouDatabaseHelper.SettingsRec("CustomerEmail", "");
        databaseHelper.updateSetting(connection, record);

        databaseHelper.closeConnection(connection);
    }

    /**
     * Save (current) ordering conditions from server to settings object and local DB
     * @param response Server response containing JSON with settings
     * @throws Exception
     */
    protected void applyOrderingSettingsFromResponse(JSONObject response) throws Exception {
        JSONObject jsonSettings = response.getJSONObject("settings");
        SQLiteDatabase connection = databaseHelper.getConnection();
        int value;
        FondouDatabaseHelper.SettingsRec record;

        value = jsonSettings.getInt("minimumTimeFrameSimple");
        settings.minimumTimeFrameSimple = value;
        record = new FondouDatabaseHelper.SettingsRec("MinimumTimeFrameSimple", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("minimumTimeFrameAdvanced");
        settings.minimumTimeFrameAdvanced = value;
        record = new FondouDatabaseHelper.SettingsRec("MinimumTimeFrameAdvanced", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("orderingStartHour");
        settings.orderingStartHour = value;
        record = new FondouDatabaseHelper.SettingsRec("OrderingStartHour", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("orderingStartMinute");
        settings.orderingStartMinute = value;
        record = new FondouDatabaseHelper.SettingsRec("OrderingStartMinute", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("orderingFinishHour");
        settings.orderingFinishHour = value;
        record = new FondouDatabaseHelper.SettingsRec("OrderingFinishHour", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("orderingFinishMinute");
        settings.orderingFinishMinute = value;
        record = new FondouDatabaseHelper.SettingsRec("OrderingFinishMinute", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("maxNoDepositCostForSimpleOrder");
        settings.maxNoDepositCostForSimpleOrder = value;
        record = new FondouDatabaseHelper.SettingsRec("MaxNoDepositCostForSimpleOrder", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        value = jsonSettings.getInt("maxNoDepositCostForAdvancedOrder");
        settings.maxNoDepositCostForAdvancedOrder = value;
        record = new FondouDatabaseHelper.SettingsRec("MaxNoDepositCostForAdvancedOrder", String.valueOf(value));
        databaseHelper.updateSetting(connection, record);

        databaseHelper.closeConnection(connection);
    }
}



