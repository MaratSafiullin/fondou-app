package nz.fondou.fondou;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Database handler. CRUD. Supposed to be used only by DataModel object
 */
public class FondouDatabaseHelper extends SQLiteOpenHelper {
    //
    private Context context;
    // database name
    private static final String DATABASE_NAME = "fondou.db";
    // database version
    private static final int DATABASE_VERSION = 1;
    // ordering types for selection from categories list
    protected static final int ORDERING_SIMPLE = 1, ORDERING_ADVANCED = 2, ORDERING_BOTH = 3;

    //**********************************************************************************************

    public FondouDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        //create tables
        createDatabase(database);

        //fill settings table with default settings values
        ContentValues values = new ContentValues();
        values.put("name", "CustomerId");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "CustomerToken");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "CustomerActive");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "CustomerPhone");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "CustomerName");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "CustomerEmail");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "GCMid");
        values.put("value", "");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "MenuTimestamp");
        values.put("value", "1970-01-01 00:00:00");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "OrdersTimestamp");
        values.put("value", "1970-01-01 00:00:00");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "MinimumTimeFrameSimple");
        values.put("value", "30");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "MinimumTimeFrameAdvanced");
        values.put("value", "2");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "OrderingStartHour");
        values.put("value", "7");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "OrderingStartMinute");
        values.put("value", "0");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "OrderingFinishHour");
        values.put("value", "14");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "OrderingFinishMinute");
        values.put("value", "30");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "MaxNoDepositCostForSimpleOrder");
        values.put("value", "20");
        database.insert("tbl_settings", null, values);
        //
        values.put("name", "MaxNoDepositCostForAdvancedOrder");
        values.put("value", "60");
        database.insert("tbl_settings", null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {
        //NOT USED
    }

    /**
     * Create database tables
     */
    protected void createDatabase(SQLiteDatabase database){
        String createSettingsTable = "CREATE TABLE [tbl_settings](\n" +
                    "[_id] [integer] PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                    "[name] [varchar](50) NOT NULL,\n" +
                    "[value] [varchar](200) NOT NULL\n" +
                ");";
        database.execSQL(createSettingsTable);

        String createMenuCategoriesTable = "CREATE TABLE [tbl_menu_categories](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[name] [varchar](50) NOT NULL,\n" +
                    "[ordering_simple] [bit] NOT NULL,\n" +
                    "[ordering_advanced] [bit] NOT NULL\n" +
                ");";
        database.execSQL(createMenuCategoriesTable);

        String createMenuItemsTable = "CREATE TABLE [tbl_menu_items](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[category_id] [integer] NOT NULL,\n" +
                    "[name] [varchar](100) NOT NULL,\n" +
                    "[size] [varchar](50) NOT NULL,\n" +
                    "[description] [varchar](500) NOT NULL,\n" +
                    "[size_num] [integer] NOT NULL,\n" +
                    "[price] [integer] NOT NULL,\n" +
                    "[active] [bit] NOT NULL\n" +
                ");";
        database.execSQL(createMenuItemsTable);

        String createOptionsTable = "CREATE TABLE [tbl_options](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[name] [varchar](50) NOT NULL\n" +
                ");";
        database.execSQL(createOptionsTable);

        String createOptionsValuesTable = "CREATE TABLE [tbl_options_values](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[option_id] [integer] NOT NULL,\n" +
                    "[value_order] [integer] NOT NULL,\n" +
                    "[value] [varchar](50) NOT NULL,\n" +
                    "[price_modifier] [integer] NOT NULL\n" +
                ");";
        database.execSQL(createOptionsValuesTable);

        String createCategoriesOptionsTable = "CREATE TABLE [tbl_categories_options](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[category_id] [integer] NOT NULL,\n" +
                    "[option_id] [integer] NOT NULL\n" +
                ");";
        database.execSQL(createCategoriesOptionsTable);

        String createOrdersTable = "CREATE TABLE [tbl_orders](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[order_time] [varchar](20) NOT NULL,\n" +
                    "[delivery_time] [varchar](20) NOT NULL,\n" +
                    "[address] [varchar](200) NOT NULL,\n" +
                    "[advanced] [bit] NOT NULL,\n" +
                    "[status] [varchar](20) NOT NULL,\n" +
                    "[total_price] [integer] NOT NULL\n" +
                ");";
        database.execSQL(createOrdersTable);

        String createOrdersItemsTable = "CREATE TABLE [tbl_orders_items](\n" +
                    "[id] [integer] PRIMARY KEY NOT NULL,\n" +
                    "[order_id] [integer] NOT NULL,\n" +
                    "[count] [integer] NOT NULL,\n" +
                    "[name] [varchar](150) NOT NULL,\n" +
                    "[description] [varchar](300) NOT NULL,\n" +
                    "[price] [integer] NOT NULL\n" +
                ");";
        database.execSQL(createOrdersItemsTable);
    }

    //**********************************************************************************************

    //Class to store Settings Record
    protected static class SettingsRec {
        public String name;
        public String value;

        public SettingsRec(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    //Class to store Menu Category Record
    protected static class MenuCategoryRec {
        public int id;
        public String name;
        public boolean simpleOrdering;
        public boolean advancedOrdering;

        public MenuCategoryRec(int id, String name, boolean simpleOrdering, boolean advancedOrdering) {
            this.id = id;
            this.name = name;
            this.simpleOrdering = simpleOrdering;
            this.advancedOrdering = advancedOrdering;
        }
    }

    //Class to store Menu Item Record
    protected static class MenuItemRec {
        public int id;
        public int categoryId;
        public String name;
        public String size;
        public String description;
        public int sizeNum;
        public int price;
        public boolean active;

        public MenuItemRec(int id, int categoryId, String name, String size, String description, int sizeNum, int price, boolean active) {
            this.id = id;
            this.categoryId = categoryId;
            this.name = name;
            this.size = size;
            this.description = description;
            this.sizeNum = sizeNum;
            this.price = price;
            this.active = active;
        }
    }

    //Class to store Option Record
    protected static class OptionRec {
        public int id;
        public String name;

        public OptionRec(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    //Class to store Category Option Record
    protected static class CategoryOptionRec {
        public int id;
        public int categoryId;
        public int optionId;

        public CategoryOptionRec(int id, int categoryId, int optionId) {
            this.id = id;
            this.categoryId = categoryId;
            this.optionId = optionId;
        }
    }

    //Class to store Option Value Record
    protected static class OptionValueRec {
        public int id;
        public int optionId;
        public int valueOrder;
        public String value;
        public int priceModifier;

        public OptionValueRec(int id, int optionId, int valueOrder, String value, int priceModifier) {
            this.id = id;
            this.optionId = optionId;
            this.valueOrder = valueOrder;
            this.value = value;
            this.priceModifier = priceModifier;
        }
    }

    //Class to store Order Record
    protected static class OrderRec {
        public int id;
        public String orderTime;
        public String deliveryTime;
        public String address;
        public boolean advanced;
        public String status;
        public int totalPrice;

        public OrderRec(int id, String orderTime, String deliveryTime, String address, boolean advanced, String status, int totalPrice) {
            this.id = id;
            this.orderTime = orderTime;
            this.deliveryTime = deliveryTime;
            this.address = address;
            this.advanced = advanced;
            this.status = status;
            this.totalPrice = totalPrice;
        }
    }

    //Class to store Order Item Record
    protected static class OrderItemRec {
        public int id;
        public int orderId;
        public int count;
        public String name;
        public String description;
        public int price;

        public OrderItemRec(int id, int orderId, int count, String name, String description, int price) {
            this.id = id;
            this.orderId = orderId;
            this.count = count;
            this.name = name;
            this.description = description;
            this.price = price;
        }
    }

    //**********************************************************************************************

    /**
     * Get connection to DB
     * @return connection
     */
    protected SQLiteDatabase getConnection(){
        return this.getWritableDatabase();
    }

    /**
     * Close connection to DB
     */
    protected void closeConnection(SQLiteDatabase connection){
        connection.close();
    }

    //**********************************************************************************************

    /**
     * Read list of settings
     * @param connection DB connection
     * @return List or records
     * @throws Exception
     */
    protected ArrayList<SettingsRec> getSettingsList(SQLiteDatabase connection) throws Exception {
        ArrayList<SettingsRec> result = new ArrayList<>();
        Cursor cursor = connection.query("tbl_settings", new String[]{"name", "value"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String value = cursor.getString(cursor.getColumnIndex("value"));

            result.add(new SettingsRec(name, value));
        }
        cursor.close();
        return result;
    }

    /**
     * Update setting record
     * @param connection DB connection
     * @param record Setting record
     * @throws Exception
     */
    protected void updateSetting(SQLiteDatabase connection, SettingsRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("value", record.value);
        connection.update("tbl_settings", values, "name = ?", new String[]{record.name});
    }

    //**********************************************************************************************

    /**
     * Check if Menu Category Exists
     * @param connection Opened connection to DB
     * @param menuCategoryId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isMenuCategoryExists(SQLiteDatabase connection, int menuCategoryId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_menu_categories", new String[]{"id"}, "id = ?", new String[]{String.valueOf(menuCategoryId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Menu Category record
     * @param connection Opened connection to DB
     * @param record New category record
     * @throws Exception
     */
    protected void addMenuCategory(SQLiteDatabase connection, MenuCategoryRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("name", record.name);
        values.put("ordering_simple", record.simpleOrdering);
        values.put("ordering_advanced", record.advancedOrdering);
        connection.insert("tbl_menu_categories", null, values);
    }

    /**
     * Update Menu Category record
     * @param connection Opened connection to DB
     * @param record Category record
     * @throws Exception
     */
    protected void updateMenuCategory(SQLiteDatabase connection, MenuCategoryRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("name", record.name);
        values.put("ordering_simple", record.simpleOrdering);
        values.put("ordering_advanced", record.advancedOrdering);
        connection.update("tbl_menu_categories", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteMenuCategoriesNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_menu_categories WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    //**********************************************************************************************

    /**
     * Check if Menu Item Exists
     * @param connection Opened connection to DB
     * @param menuItemId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isMenuItemExists(SQLiteDatabase connection, int menuItemId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_menu_items", new String[]{"id"}, "id = ?", new String[]{String.valueOf(menuItemId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Menu Item record
     * @param connection Opened connection to DB
     * @param record New item record
     * @throws Exception
     */
    protected void addMenuItem(SQLiteDatabase connection, MenuItemRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("category_id", record.categoryId);
        values.put("name", record.name);
        values.put("size", record.size);
        values.put("description", record.description);
        values.put("size_num", record.sizeNum);
        values.put("price", record.price);
        if (record.active) {
            values.put("active", 1);
        } else {
            values.put("active", 0);
        }
        connection.insert("tbl_menu_items", null, values);
    }

    /**
     * Update Menu Item record
     * @param connection Opened connection to DB
     * @param record Item record
     * @throws Exception
     */
    protected void updateMenuItem(SQLiteDatabase connection, MenuItemRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("category_id", record.categoryId);
        values.put("name", record.name);
        values.put("size", record.size);
        values.put("description", record.description);
        values.put("size_num", record.sizeNum);
        values.put("price", record.price);
        if (record.active) {
            values.put("active", 1);
        } else {
            values.put("active", 0);
        }
        connection.update("tbl_menu_items", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteMenuItemsNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_menu_items WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    //**********************************************************************************************

    /**
     * Check if Option Exists
     * @param connection Opened connection to DB
     * @param optionId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isOptionExists(SQLiteDatabase connection, int optionId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_options", new String[]{"id"}, "id = ?", new String[]{String.valueOf(optionId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Option record
     * @param connection Opened connection to DB
     * @param record New Option record
     * @throws Exception
     */
    protected void addOption(SQLiteDatabase connection, OptionRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("name", record.name);

        connection.insert("tbl_options", null, values);
    }

    /**
     * Update Option record
     * @param connection Opened connection to DB
     * @param record Option record
     * @throws Exception
     */
    protected void updateOption(SQLiteDatabase connection, OptionRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("name", record.name);

        connection.update("tbl_options", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteOptionsNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_options WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    //**********************************************************************************************

    /**
     * Check if Category Option record Exists
     * @param connection Opened connection to DB
     * @param optionId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isCategoryOptionExists(SQLiteDatabase connection, int optionId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_categories_options", new String[]{"id"}, "id = ?", new String[]{String.valueOf(optionId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Category Option record
     * @param connection Opened connection to DB
     * @param record New Category Option record
     * @throws Exception
     */
    protected void addCategoryOption(SQLiteDatabase connection, CategoryOptionRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("category_id", record.categoryId);
        values.put("option_id", record.optionId);

        connection.insert("tbl_categories_options", null, values);
    }

    /**
     * Update Category Option record
     * @param connection Opened connection to DB
     * @param record Option record
     * @throws Exception
     */
    protected void updateCategoryOption(SQLiteDatabase connection, CategoryOptionRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("category_id", record.categoryId);
        values.put("option_id", record.optionId);

        connection.update("tbl_categories_options", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteCategoryOptionsNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_categories_options WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    //**********************************************************************************************

    /**
     * Check if Option Value Exists
     * @param connection Opened connection to DB
     * @param optionValueId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isOptionValueExists(SQLiteDatabase connection, int optionValueId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_options_values", new String[]{"id"}, "id = ?", new String[]{String.valueOf(optionValueId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Option Value record
     * @param connection Opened connection to DB
     * @param record New Option Value record
     * @throws Exception
     */
    protected void addOptionValue(SQLiteDatabase connection, OptionValueRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("option_id", record.optionId);
        values.put("value_order", record.valueOrder);
        values.put("value", record.value);
        values.put("price_modifier", record.priceModifier);

        connection.insert("tbl_options_values", null, values);
    }

    /**
     * Update Option Value record
     * @param connection Opened connection to DB
     * @param record Option Value record
     * @throws Exception
     */
    protected void updateOptionValue(SQLiteDatabase connection, OptionValueRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("option_id", record.optionId);
        values.put("value_order", record.valueOrder);
        values.put("value", record.value);
        values.put("price_modifier", record.priceModifier);

        connection.update("tbl_options_values", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteOptionValuesNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_options_values WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    //**********************************************************************************************

    /**
     * Check if Order Exists
     * @param connection Opened connection to DB
     * @param orderId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isOrderExists(SQLiteDatabase connection, int orderId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_orders", new String[]{"id"}, "id = ?", new String[]{String.valueOf(orderId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Order record
     * @param connection Opened connection to DB
     * @param record New Order record
     * @throws Exception
     */
    protected void addOrder(SQLiteDatabase connection, OrderRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("order_time", record.orderTime);
        values.put("delivery_time", record.deliveryTime);
        values.put("address", record.address);
        values.put("advanced", record.advanced);
        values.put("status", record.status);
        values.put("total_price", record.totalPrice);

        connection.insert("tbl_orders", null, values);
    }

    /**
     * Update Order record
     * @param connection Opened connection to DB
     * @param record Order record
     * @throws Exception
     */
    protected void updateOrder(SQLiteDatabase connection, OrderRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("order_time", record.orderTime);
        values.put("delivery_time", record.deliveryTime);
        values.put("address", record.address);
        values.put("advanced", record.advanced);
        values.put("status", record.status);
        values.put("total_price", record.totalPrice);

        connection.update("tbl_orders", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteOrdersNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_orders WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    /**
     * Delete Order record and all Order Item records related to it
     * @param connection Opened connection to DB
     * @param orderId List of record Ids to keep
     * @throws Exception
     */
    protected void deleteOrderWithItems(SQLiteDatabase connection, int orderId) throws Exception {
        connection.delete("tbl_orders_items", "order_id = ?", new String[]{String.valueOf(orderId)});
        connection.delete("tbl_orders", "id = ?", new String[]{String.valueOf(orderId)});
    }

    /**
     * Delete all Order records
     * @param connection Opened connection to DB
     * @throws Exception
     */
    protected void deleteAllOrders(SQLiteDatabase connection) throws Exception {
        connection.delete("tbl_orders", null, null);
    }

    //**********************************************************************************************

    /**
     * Check if Order Item Exists
     * @param connection Opened connection to DB
     * @param orderItemId Id in database
     * @return True if exists, false otherwise
     * @throws Exception
     */
    protected boolean isOrderItemExists(SQLiteDatabase connection, int orderItemId) throws Exception {
        boolean result;
        Cursor cursor = connection.query("tbl_orders_items", new String[]{"id"}, "id = ?", new String[]{String.valueOf(orderItemId)}, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        } else {
            result = false;
        }
        cursor.close();
        return result;
    }

    /**
     * Create new Order Item record
     * @param connection Opened connection to DB
     * @param record New Order Item record
     * @throws Exception
     */
    protected void addOrderItem(SQLiteDatabase connection, OrderItemRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("id", record.id);
        values.put("order_id", record.orderId);
        values.put("count", record.count);
        values.put("name", record.name);
        values.put("description", record.description);
        values.put("price", record.price);

        connection.insert("tbl_orders_items", null, values);
    }

    /**
     * Update Order Item record
     * @param connection Opened connection to DB
     * @param record Order Item record
     * @throws Exception
     */
    protected void updateOrderItem(SQLiteDatabase connection, OrderItemRec record) throws Exception {
        ContentValues values = new ContentValues();
        values.put("order_id", record.orderId);
        values.put("count", record.count);
        values.put("name", record.name);
        values.put("description", record.description);
        values.put("price", record.price);

        connection.update("tbl_orders_items", values, "id = ?", new String[]{String.valueOf(record.id)});
    }

    /**
     * Delete records with Ids not in list
     * @param connection Opened connection to DB
     * @param list List of record Ids to keep
     * @throws Exception
     */
    protected void deleteOrderItemsNotInList(SQLiteDatabase connection, ArrayList<Integer> list) throws Exception {
        StringBuilder conditionBuilder = new StringBuilder("");
        if (list.size() > 0) {
            conditionBuilder.append(list.get(0));
            for (int i = 1; i < list.size(); i++) {
                conditionBuilder.append(", " + list.get(i));
            }
        }
        String query = "DELETE FROM tbl_orders_items WHERE NOT (id IN (" + conditionBuilder.toString() + "))";
        connection.execSQL(query);
    }

    /**
     * Delete all Order Item records
     * @param connection Opened connection to DB
     * @throws Exception
     */
    protected void deleteAllOrderItems(SQLiteDatabase connection) throws Exception {
        connection.delete("tbl_orders_items", null, null);
    }

    //**********************************************************************************************

    /**
     * Get a list of Menu Categories sorted by name
     * @param connection Opened connection to DB
     * @param orderingType Type of Order to choose categories selection mode
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<MenuCategoryRec> getMenuCategoriesList(SQLiteDatabase connection, int orderingType) throws Exception {
        ArrayList<MenuCategoryRec> result = new ArrayList<>();
        String conditions;
        switch (orderingType) {
            case ORDERING_SIMPLE:
                conditions = "ordering_simple = 1";
                break;
            case ORDERING_ADVANCED:
                conditions = "ordering_advanced = 1";
                break;
            case ORDERING_BOTH:
                conditions = "ordering_simple = 1 AND ordering_advanced = 1";
                break;
            default:
                conditions = null;
                break;
        }
        Cursor cursor = connection.query("tbl_menu_categories",
                new String[]{"id", "name", "ordering_simple", "ordering_advanced"}, conditions, null, null, null, "name");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            boolean simpleOrdering = (cursor.getInt(cursor.getColumnIndex("ordering_simple")) == 1);
            boolean advancedOrdering = (cursor.getInt(cursor.getColumnIndex("ordering_advanced")) == 1);
            result.add(new MenuCategoryRec(id, name, simpleOrdering, advancedOrdering));
        }
        cursor.close();
        return result;
    }

    /**
     * Get a list of Menu Items of a particular Category sorted by name and size
     * @param connection Opened connection to DB
     * @param categoryId Id of the category
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<MenuItemRec> getCategoryItemsList(SQLiteDatabase connection, int categoryId) throws Exception {
        ArrayList<MenuItemRec> result = new ArrayList<>();
        Cursor cursor = connection.query("tbl_menu_items",
                new String[]{"id", "name", "size", "description", "size_num", "price"}, "category_id = ? AND active = 1", new String[]{String.valueOf(categoryId)}, null, null, "name, size_num");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String size = cursor.getString(cursor.getColumnIndex("size"));
            String description = cursor.getString(cursor.getColumnIndex("description"));
            int sizeNum = cursor.getInt(cursor.getColumnIndex("size_num"));
            int price = cursor.getInt(cursor.getColumnIndex("price"));

            result.add(new MenuItemRec(id, categoryId, name, size, description, sizeNum, price, true));
        }
        cursor.close();
        return result;
    }

    /**
     * Get a list of Options of a particular Category sorted by id
     * @param connection Opened connection to DB
     * @param categoryId Id of the category
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<OptionRec> getCategoryOptionsList(SQLiteDatabase connection, int categoryId) throws Exception {
        ArrayList<OptionRec> result = new ArrayList<>();
        String query = "SELECT opt.id, opt.name FROM tbl_options AS opt " +
                "INNER JOIN tbl_categories_options AS cop ON cop.option_id = opt.id " +
                "WHERE cop.category_id = " + String.valueOf(categoryId) + " " +
                "ORDER BY opt.id";
        Cursor cursor = connection.rawQuery(query, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String name = cursor.getString(cursor.getColumnIndex("name"));

            result.add(new OptionRec(id, name));
        }
        cursor.close();
        return result;
    }

    /**
     * Get a list of Option Values of a particular Option sorted by "order" field and id
     * @param connection Opened connection to DB
     * @param optionId Id of the Option
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<OptionValueRec> getOptionValuesList(SQLiteDatabase connection, int optionId) throws Exception {
        ArrayList<OptionValueRec> result = new ArrayList<>();
        Cursor cursor = connection.query("tbl_options_values",
                new String[]{"id", "value_order", "value", "price_modifier"}, "option_id = ?", new String[]{String.valueOf(optionId)}, null, null, "value_order, id");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            int valueOrder = cursor.getInt(cursor.getColumnIndex("value_order"));
            String value = cursor.getString(cursor.getColumnIndex("value"));
            int priceModifier = cursor.getInt(cursor.getColumnIndex("price_modifier"));

            result.add(new OptionValueRec(id, optionId, valueOrder, value, priceModifier));
        }
        cursor.close();
        return result;
    }

    /**
     * Get a list of Orders with particular status sorted by order time (time of posting)
     * @param connection Opened connection to DB
     * @param status Status to select orders
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<OrderRec> getOrdersList(SQLiteDatabase connection, String status) throws Exception {
        ArrayList<OrderRec> result = new ArrayList<>();
        Cursor cursor = connection.query("tbl_orders",
                new String[]{"id", "order_time", "delivery_time", "address", "advanced", "total_price"}, "status = ?",
                new String[]{status}, null, null, "order_time");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String orderTime = cursor.getString(cursor.getColumnIndex("order_time"));
            String deliveryTime = cursor.getString(cursor.getColumnIndex("delivery_time"));
            String address = cursor.getString(cursor.getColumnIndex("address"));
            boolean advanced = (cursor.getInt(cursor.getColumnIndex("advanced")) == 1);
            int totalPrice = cursor.getInt(cursor.getColumnIndex("total_price"));

            result.add(new OrderRec(id, orderTime, deliveryTime, address, advanced, status, totalPrice));
        }
        cursor.close();
        return result;
    }

    /**
     * Get a list of Order Items of a particular Order sorted by id
     * @param connection Opened connection to DB
     * @param orderId Id of the Order
     * @return List of categories
     * @throws Exception
     */
    protected ArrayList<OrderItemRec> getOrderItemsList(SQLiteDatabase connection, int orderId) throws Exception {
        ArrayList<OrderItemRec> result = new ArrayList<>();
        Cursor cursor = connection.query("tbl_orders_items",
                new String[]{"id", "count", "name", "description", "price"}, "order_id = ?",
                new String[]{String.valueOf(orderId)}, null, null, "id");
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            int count = cursor.getInt(cursor.getColumnIndex("count"));
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String description = cursor.getString(cursor.getColumnIndex("description"));
            int price = cursor.getInt(cursor.getColumnIndex("price"));

            result.add(new OrderItemRec(id, orderId, count, name, description, price));
        }
        cursor.close();
        return result;
    }
}
