package nz.fondou.fondou;

/**
 * Shared preferences constants
 */
public class PreferencesAndConstants {
    //GCM registration service constants
    public static final String GCM_REGISTRATION_SUCCESSFUL = "GCM Registration Successful";
    public static final String GCM_ID = "GCM Id";
    public static final String REGISTRATION_COMPLETE = "GCM Registration Complete";
    public static final String ORDER_STATUS_CHANGE_NOTIFICATION  = "OrderStatusChangeNotification";
}
