package nz.fondou.fondou;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Class to implement GMC message listener
 */
public class GcmMessageListenerService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        //get message data
        String message = data.getString("message");
        String title = data.getString("title");

        //show notification for user if there is valid login data in app settings
        FondouDatabaseHelper databaseHelper = new FondouDatabaseHelper(this);
        DataModel dataModel = new DataModel(databaseHelper);
        try {
            dataModel.readSettings();
            if ((dataModel.settings.customerId != 0) && (!dataModel.settings.customerToken.equals(""))) {
                sendNotification(message, title);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Show notification for user
     * @param text GCM message text
     * @param title GCM message title
     */
    private void sendNotification(String text, String title) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(PreferencesAndConstants.ORDER_STATUS_CHANGE_NOTIFICATION, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
