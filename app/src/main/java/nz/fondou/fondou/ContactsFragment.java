package nz.fondou.fondou;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * An informational fragment. Has links to social media related to fond(ou) cafe
 */
public class ContactsFragment extends Fragment {

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        //facebook
        ImageButton buttonContactsFacebook = (ImageButton) view.findViewById(R.id.buttonContactsFacebook);
        buttonContactsFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/fondoucafe/"));
                startActivity(browserIntent);
            }
        });
        //instagram
        ImageButton buttonContactsInstagram = (ImageButton) view.findViewById(R.id.buttonContactsInstagram);
        buttonContactsInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/fondoucafe/"));
                startActivity(browserIntent);
            }
        });
        //tripadvisor
        ImageButton buttonContactsTripAdvisor = (ImageButton) view.findViewById(R.id.buttonContactsTripAdvisor);
        buttonContactsTripAdvisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.tripadvisor.co.nz/Restaurant_Review-g255106-d7624036-Reviews-Fond_Ou-Auckland_North_Island.html"));
                startActivity(browserIntent);
            }
        });

        return view;
    }

}
